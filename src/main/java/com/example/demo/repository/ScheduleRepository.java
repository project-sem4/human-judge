package com.example.demo.repository;

import com.example.demo.entity.Schedule;
import com.example.demo.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

public interface ScheduleRepository extends JpaRepository<Schedule, Long>, JpaSpecificationExecutor<Schedule> {
    Optional<Schedule> findScheduleById(long id);

    List<Schedule> findByClazzIdAndTeacherIdAndDate(long teacherId, long clazzId, String date);

    List<Schedule> findByClazzId(long id);

    List<Schedule> findBySubjectId(long id);

    List<Schedule> findByDateAndClazzId(String date, long id);
}
