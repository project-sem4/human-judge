package com.example.demo.repository;

import com.example.demo.entity.Clazz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClazzRepository extends JpaRepository<Clazz, Long>, JpaSpecificationExecutor<Clazz> {
    Optional<Clazz> findClazzById(long id);
    //List<Clazz> findClazzByTeacher_Id(long id);
}
