package com.example.demo.repository;

import com.example.demo.entity.Credential;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CredentialRepository extends JpaRepository<Credential, String> {
    Optional<Credential> findByAccessToken(String accessToken);
}

