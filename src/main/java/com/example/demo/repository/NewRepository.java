package com.example.demo.repository;

import com.example.demo.entity.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

public interface NewRepository extends JpaRepository<Article, Long>, JpaSpecificationExecutor<Article> {
    List<Article> getAllByStatus(int status);
    Optional<Article> findNewById(long id);
}
