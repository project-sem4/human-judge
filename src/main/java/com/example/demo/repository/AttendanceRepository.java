package com.example.demo.repository;

import com.example.demo.entity.Attendance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AttendanceRepository extends JpaRepository<Attendance, Long>, JpaSpecificationExecutor<Attendance> {
    Optional<Attendance> findAttendanceById(long id);
    List<Attendance> findByScheduleId(long schedule);
    List<Attendance> findByStudentId(long student);
    Optional<Attendance> findByScheduleIdAndStudentId(long scheduleId, long studentId);
    List<Attendance> findByStudentIdAndStatus(long id, int status);
}

