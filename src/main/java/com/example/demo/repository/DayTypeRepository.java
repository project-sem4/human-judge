package com.example.demo.repository;

import com.example.demo.entity.DayType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface DayTypeRepository extends JpaRepository<DayType, Long>, JpaSpecificationExecutor<DayType> {
    Optional<DayType> findDayTypeById(long id);
}
