package com.example.demo.dto;

import com.example.demo.entity.Semester;
import com.example.demo.util.StringUtil;

public class SemesterDTO {

    private String id;
    private String name;
    private String status;
    private String createdAt;
    private String updatedAt;

    public SemesterDTO() {
    }

    public SemesterDTO(Semester semester) {
        this.id = String.valueOf(semester.getId());
        this.name = semester.getName();
        this.status = new StringUtil().convertIntStatusToString(semester.getStatus());
        this.createdAt = new StringUtil().convertMilToString(semester.getCreatedAt());
        this.updatedAt = new StringUtil().convertMilToString(semester.getUpdatedAt());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
