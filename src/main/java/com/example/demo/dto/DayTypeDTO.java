package com.example.demo.dto;

import com.example.demo.entity.DayType;

public class DayTypeDTO {
    private String id;
    private String name;

    public DayTypeDTO() {
    }
    public DayTypeDTO(DayType dayType) {
        this.id = String.valueOf(dayType.getId());
        this.name = dayType.getName();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
