package com.example.demo.dto.context;

import com.example.demo.dto.AccountDTO;
import com.example.demo.dto.AdminDTO;
import com.example.demo.dto.CredentialDTO;

public class AdminContext {
    private AccountDTO accountDTO;
    private AdminDTO adminDTO;
    private CredentialDTO credentialDTO;

    public AdminContext() {
    }

    public AdminContext(AccountDTO accountDTO, AdminDTO adminDTO) {
        this.accountDTO = accountDTO;
        this.adminDTO = adminDTO;
    }

    public AdminContext(AccountDTO accountDTO, AdminDTO adminDTO, CredentialDTO credentialDTO) {
        this.accountDTO = accountDTO;
        this.adminDTO = adminDTO;
        this.credentialDTO = credentialDTO;
    }

    public AccountDTO getAccountDTO() {
        return accountDTO;
    }

    public void setAccountDTO(AccountDTO accountDTO) {
        this.accountDTO = accountDTO;
    }

    public AdminDTO getAdminDTO() {
        return adminDTO;
    }

    public void setAdminDTO(AdminDTO adminDTO) {
        this.adminDTO = adminDTO;
    }

    public CredentialDTO getCredentialDTO() {
        return credentialDTO;
    }

    public void setCredentialDTO(CredentialDTO credentialDTO) {
        this.credentialDTO = credentialDTO;
    }
}
