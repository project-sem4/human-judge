package com.example.demo.dto.context;

import com.example.demo.dto.AttendanceDTO2;
import com.example.demo.entity.Attendance;

import java.util.Set;

public class AttendanceScheduleContext {
    private String scheduleId;
    private String scheduleDate;
    private String clazzId;
    private String clazzName;
    private String roomId;
    private String roomName;
    private String slotId;
    private String slotName;
    private Set<AttendanceDTO2> attendanceDTOS;

    public AttendanceScheduleContext() {
    }

    public AttendanceScheduleContext(Attendance attendance, Set<AttendanceDTO2> attendanceDTOS) {
        this.scheduleId = String.valueOf(attendance.getSchedule().getId());
        this.scheduleDate = attendance.getSchedule().getDate();
        this.clazzId = String.valueOf(attendance.getSchedule().getClazz().getId());
        this.clazzName = attendance.getSchedule().getClazz().getName();
        this.roomId = String.valueOf(attendance.getSchedule().getRoom().getId());
        this.roomName = attendance.getSchedule().getRoom().getName();
        this.slotId = String.valueOf(attendance.getSchedule().getSlot().getId());
        this.slotName = attendance.getSchedule().getSlot().getName();
        this.attendanceDTOS = attendanceDTOS;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public String getClazzId() {
        return clazzId;
    }

    public void setClazzId(String clazzId) {
        this.clazzId = clazzId;
    }

    public String getClazzName() {
        return clazzName;
    }

    public void setClazzName(String clazzName) {
        this.clazzName = clazzName;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getSlotId() {
        return slotId;
    }

    public void setSlotId(String slotId) {
        this.slotId = slotId;
    }

    public String getSlotName() {
        return slotName;
    }

    public void setSlotName(String slotName) {
        this.slotName = slotName;
    }

    public Set<AttendanceDTO2> getAttendanceDTOS() {
        return attendanceDTOS;
    }

    public void setAttendanceDTOS(Set<AttendanceDTO2> attendanceDTOS) {
        this.attendanceDTOS = attendanceDTOS;
    }
}
