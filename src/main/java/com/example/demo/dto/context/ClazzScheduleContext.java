package com.example.demo.dto.context;

import com.example.demo.dto.ClassDTO;
import com.example.demo.dto.ScheduleDTO;

import java.util.List;
import java.util.Set;

public class ClazzScheduleContext {
    private ClassDTO classDTO;
    private Set<ScheduleDTO> listScheduleDTO;

    public ClazzScheduleContext() {
    }

    public ClazzScheduleContext(ClassDTO classDTO, Set<ScheduleDTO> listScheduleDTO) {
        this.classDTO = classDTO;
        this.listScheduleDTO = listScheduleDTO;
    }

    public ClassDTO getClassDTO() {
        return classDTO;
    }

    public void setClassDTO(ClassDTO classDTO) {
        this.classDTO = classDTO;
    }

    public Set<ScheduleDTO> getListScheduleDTO() {
        return listScheduleDTO;
    }

    public void setListScheduleDTO(Set<ScheduleDTO> listScheduleDTO) {
        this.listScheduleDTO = listScheduleDTO;
    }
}
