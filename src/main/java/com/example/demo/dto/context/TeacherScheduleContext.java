package com.example.demo.dto.context;

import com.example.demo.dto.ScheduleDTO;
import com.example.demo.dto.TeacherDTO;

import java.util.List;

public class TeacherScheduleContext {
    private TeacherDTO teacherDTO;
    private List<ScheduleDTO> listScheduleDTO;

    public TeacherScheduleContext() {
    }

    public TeacherScheduleContext(TeacherDTO teacherDTO, List<ScheduleDTO> listScheduleDTO) {
        this.teacherDTO = teacherDTO;
        this.listScheduleDTO = listScheduleDTO;
    }

    public TeacherDTO getTeacherDTO() {
        return teacherDTO;
    }

    public void setTeacherDTO(TeacherDTO teacherDTO) {
        this.teacherDTO = teacherDTO;
    }

    public List<ScheduleDTO> getListScheduleDTO() {
        return listScheduleDTO;
    }

    public void setListScheduleDTO(List<ScheduleDTO> listScheduleDTO) {
        this.listScheduleDTO = listScheduleDTO;
    }
}
