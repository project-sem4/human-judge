package com.example.demo.dto.context;

import com.example.demo.dto.AttendanceDTO;
import com.example.demo.dto.StudentDTO;

import java.util.List;
import java.util.Set;

public class StudentAttendanceContext {
    private StudentDTO studentDTO;
    private Set<AttendanceDTO> attendanceDTOList;

    public StudentAttendanceContext(StudentDTO studentDTO, Set<AttendanceDTO> attendanceDTOList) {
        this.studentDTO = studentDTO;
        this.attendanceDTOList = attendanceDTOList;
    }

    public StudentDTO getStudentDTO() {
        return studentDTO;
    }

    public void setStudentDTO(StudentDTO studentDTO) {
        this.studentDTO = studentDTO;
    }

    public Set<AttendanceDTO> getAttendanceDTOList() {
        return attendanceDTOList;
    }

    public void setAttendanceDTOList(Set<AttendanceDTO> attendanceDTOList) {
        this.attendanceDTOList = attendanceDTOList;
    }
}
