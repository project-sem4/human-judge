package com.example.demo.dto.context;

import com.example.demo.dto.AccountDTO;
import com.example.demo.dto.CredentialDTO;
import com.example.demo.dto.TeacherDTO;

public class TeacherContext {
    private AccountDTO accountDTO;
    private TeacherDTO teacherDTO;
    private CredentialDTO credentialDTO;

    public TeacherContext() {
    }

    public TeacherContext(AccountDTO accountDTO, TeacherDTO teacherDTO) {
        this.accountDTO = accountDTO;
        this.teacherDTO = teacherDTO;
    }

    public TeacherContext(AccountDTO accountDTO, TeacherDTO teacherDTO, CredentialDTO credentialDTO) {
        this.accountDTO = accountDTO;
        this.teacherDTO = teacherDTO;
        this.credentialDTO = credentialDTO;
    }

    public CredentialDTO getCredentialDTO() {
        return credentialDTO;
    }

    public void setCredentialDTO(CredentialDTO credentialDTO) {
        this.credentialDTO = credentialDTO;
    }

    public AccountDTO getAccountDTO() {
        return accountDTO;
    }

    public void setAccountDTO(AccountDTO accountDTO) {
        this.accountDTO = accountDTO;
    }

    public TeacherDTO getTeacherDTO() {
        return teacherDTO;
    }

    public void setTeacherDTO(TeacherDTO teacherDTO) {
        this.teacherDTO = teacherDTO;
    }
}
