package com.example.demo.dto.context;

import com.example.demo.dto.AccountDTO;
import com.example.demo.dto.CredentialDTO;
import com.example.demo.dto.EmployeeDTO;

public class EmployeeContext {
    private AccountDTO accountDTO;
    private EmployeeDTO employeeDTO;
    private CredentialDTO credentialDTO;

    public EmployeeContext() {
    }

    public EmployeeContext(AccountDTO accountDTO, EmployeeDTO employeeDTO) {
        this.accountDTO = accountDTO;
        this.employeeDTO = employeeDTO;
    }

    public EmployeeContext(AccountDTO accountDTO, EmployeeDTO employeeDTO, CredentialDTO credentialDTO) {
        this.accountDTO = accountDTO;
        this.employeeDTO = employeeDTO;
        this.credentialDTO = credentialDTO;
    }

    public AccountDTO getAccountDTO() {
        return accountDTO;
    }

    public void setAccountDTO(AccountDTO accountDTO) {
        this.accountDTO = accountDTO;
    }

    public EmployeeDTO getEmployeeDTO() {
        return employeeDTO;
    }

    public void setEmployeeDTO(EmployeeDTO employeeDTO) {
        this.employeeDTO = employeeDTO;
    }

    public CredentialDTO getCredentialDTO() {
        return credentialDTO;
    }

    public void setCredentialDTO(CredentialDTO credentialDTO) {
        this.credentialDTO = credentialDTO;
    }
}
