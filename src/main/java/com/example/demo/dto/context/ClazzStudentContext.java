package com.example.demo.dto.context;

import com.example.demo.dto.ClassDTO;
import com.example.demo.dto.StudentDTO;

import java.util.List;

public class ClazzStudentContext {
    private ClassDTO classDTO;
    private List<StudentDTO> listStudent;

    public ClazzStudentContext() {
    }

    public ClazzStudentContext(ClassDTO classDTO, List<StudentDTO> listStudent) {
        this.classDTO = classDTO;
        this.listStudent = listStudent;
    }

    public ClassDTO getClassDTO() {
        return classDTO;
    }

    public void setClassDTO(ClassDTO classDTO) {
        this.classDTO = classDTO;
    }

    public List<StudentDTO> getListStudent() {
        return listStudent;
    }

    public void setListStudent(List<StudentDTO> listStudent) {
        this.listStudent = listStudent;
    }
}
