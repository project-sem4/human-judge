package com.example.demo.dto.context;

import com.example.demo.dto.ClassDTO;
import com.example.demo.dto.ScheduleDTO;
import com.example.demo.dto.TeacherDTO;

import java.util.List;
import java.util.Set;

public class ClassTeacherScheduleContext {
    private ClassDTO classDTO;
    private TeacherDTO teacherDTO;
    private Set<ScheduleDTO> listScheduleDTO;

    public ClassTeacherScheduleContext() {
    }

    public ClassTeacherScheduleContext(ClassDTO classDTO, TeacherDTO teacherDTO, Set<ScheduleDTO> listScheduleDTO) {
        this.classDTO = classDTO;
        this.teacherDTO = teacherDTO;
        this.listScheduleDTO = listScheduleDTO;
    }

    public ClassDTO getClassDTO() {
        return classDTO;
    }

    public void setClassDTO(ClassDTO classDTO) {
        this.classDTO = classDTO;
    }

    public TeacherDTO getTeacherDTO() {
        return teacherDTO;
    }

    public void setTeacherDTO(TeacherDTO teacherDTO) {
        this.teacherDTO = teacherDTO;
    }

    public Set<ScheduleDTO> getListScheduleDTO() {
        return listScheduleDTO;
    }

    public void setListScheduleDTO(Set<ScheduleDTO> listScheduleDTO) {
        this.listScheduleDTO = listScheduleDTO;
    }
}
