package com.example.demo.dto.context;

import com.example.demo.dto.AccountDTO;
import com.example.demo.dto.CredentialDTO;
import com.example.demo.dto.StudentDTO;

public class StudentContext {
    private AccountDTO accountDTO;
    private StudentDTO studentDTO;
    private CredentialDTO credentialDTO;

    public StudentContext() {
    }

    public StudentContext(AccountDTO accountDTO, StudentDTO studentDTO) {
        this.accountDTO = accountDTO;
        this.studentDTO = studentDTO;
    }

    public StudentContext(AccountDTO accountDTO, StudentDTO studentDTO, CredentialDTO credentialDTO) {
        this.accountDTO = accountDTO;
        this.studentDTO = studentDTO;
        this.credentialDTO = credentialDTO;
    }

    public AccountDTO getAccountDTO() {
        return accountDTO;
    }

    public void setAccountDTO(AccountDTO accountDTO) {
        this.accountDTO = accountDTO;
    }

    public StudentDTO getStudentDTO() {
        return studentDTO;
    }

    public void setStudentDTO(StudentDTO studentDTO) {
        this.studentDTO = studentDTO;
    }

    public CredentialDTO getCredentialDTO() {
        return credentialDTO;
    }

    public void setCredentialDTO(CredentialDTO credentialDTO) {
        this.credentialDTO = credentialDTO;
    }
}
