package com.example.demo.dto.context;

import com.example.demo.dto.ClassDTO;
import com.example.demo.dto.TeacherDTO;

import java.util.List;

public class ClazzTeacherContext {
    private List<TeacherDTO> teacherDTO;
    private ClassDTO classDTO;

    public ClazzTeacherContext() {
    }

    public ClazzTeacherContext(List<TeacherDTO> teacherDTO, ClassDTO classDTO) {
        this.teacherDTO = teacherDTO;
        this.classDTO = classDTO;
    }

    public List<TeacherDTO> getTeacherDTO() {
        return teacherDTO;
    }

    public void setTeacherDTO(List<TeacherDTO> teacherDTO) {
        this.teacherDTO = teacherDTO;
    }

    public ClassDTO getClassDTO() {
        return classDTO;
    }

    public void setClassDTO(ClassDTO classDTO) {
        this.classDTO = classDTO;
    }
}
