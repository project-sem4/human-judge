package com.example.demo.dto.context;

import com.example.demo.dto.ClassDTO;
import com.example.demo.dto.StudentDTO;

import java.util.List;

public class StudentClazzContext {
    private StudentDTO studentDTO;
    private List<ClassDTO> listClassDTO;

    public StudentClazzContext() {
    }

    public StudentClazzContext(StudentDTO studentDTO, List<ClassDTO> listClassDTO) {
        this.studentDTO = studentDTO;
        this.listClassDTO = listClassDTO;
    }

    public StudentDTO getStudentDTO() {
        return studentDTO;
    }

    public void setStudentDTO(StudentDTO studentDTO) {
        this.studentDTO = studentDTO;
    }

    public List<ClassDTO> getListClassDTO() {
        return listClassDTO;
    }

    public void setListClassDTO(List<ClassDTO> listClassDTO) {
        this.listClassDTO = listClassDTO;
    }
}
