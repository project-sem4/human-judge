package com.example.demo.dto.context;

import com.example.demo.dto.ClassDTO;
import com.example.demo.dto.TeacherDTO;

import java.util.List;

public class TeacherClazzContext {
    private TeacherDTO teacherDTO;
    private List<ClassDTO> listClassDTO;

    public TeacherClazzContext(TeacherDTO teacherDTO, List<ClassDTO> listClassDTO) {
        this.teacherDTO = teacherDTO;
        this.listClassDTO = listClassDTO;
    }

    public TeacherDTO getTeacherDTO() {
        return teacherDTO;
    }

    public void setTeacherDTO(TeacherDTO teacherDTO) {
        this.teacherDTO = teacherDTO;
    }

    public List<ClassDTO> getListClassDTO() {
        return listClassDTO;
    }

    public void setListClassDTO(List<ClassDTO> listClassDTO) {
        this.listClassDTO = listClassDTO;
    }
}
