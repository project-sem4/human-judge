package com.example.demo.dto.context;

import com.example.demo.dto.ClassDTO;
import com.example.demo.dto.SlotDTO;

import java.util.List;

public class ClazzSlotContext {
    private ClassDTO classDTO;
    private List<SlotDTO> listSlot;

    public ClazzSlotContext(ClassDTO classDTO, List<SlotDTO> listSlot) {
        this.classDTO = classDTO;
        this.listSlot = listSlot;
    }

    public ClassDTO getClassDTO() {
        return classDTO;
    }

    public void setClassDTO(ClassDTO classDTO) {
        this.classDTO = classDTO;
    }

    public List<SlotDTO> getListSlot() {
        return listSlot;
    }

    public void setListSlot(List<SlotDTO> listSlot) {
        this.listSlot = listSlot;
    }
}
