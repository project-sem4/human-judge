package com.example.demo.dto;

import com.example.demo.entity.Slot;
import com.example.demo.util.StringUtil;

public class SlotDTO {
    private String id;
    private String name;
    private String startTime;
    private String endTime;

    public SlotDTO() {
    }

    public SlotDTO(Slot slot) {
        this.id = String.valueOf(slot.getId());
        this.name = slot.getName();
        this.startTime = slot.getStartTime();
        this.endTime = slot.getEndTime();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
