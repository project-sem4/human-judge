package com.example.demo.dto;

import com.example.demo.entity.Article;
import com.example.demo.util.StringUtil;

public class ArticleDTO {
    private String id;
    private String title;
    private String description;
    private String categoryId;
    private String categoryName;
    private String content;
    private String adminId;
    private String adminName;
    private String status;
    private String createdAt;
    private String updatedAt;
    private String deletedAt;

    public ArticleDTO() {
    }

    public ArticleDTO(Article article) {
        this.id = String.valueOf(article.getId());
        this.title = article.getTitle();
        this.description = article.getDescription();
        this.status = new StringUtil().convertIntStatusToString(article.getStatus());
        this.createdAt = new StringUtil().convertMilToString(article.getCreatedAt());
        this.updatedAt = new StringUtil().convertMilToString(article.getUpdatedAt());
        this.deletedAt = new StringUtil().convertMilToString(article.getDeletedAt());
        this.adminId = String.valueOf(article.getAdmin().getId());
        this.adminName = article.getAdmin().getFullName();
        this.categoryId = String.valueOf(article.getCategory().getId());
        this.categoryName = article.getCategory().getName();
        this.content = article.getContent();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
