package com.example.demo.dto;

import com.example.demo.entity.Room;
import com.example.demo.util.StringUtil;

public class RoomDTO {
    private String id;
    private String name;
    private String status;
    private String createdAt;
    private String updatedAt;
    private String deletedAt;

    public RoomDTO() {
    }

    public RoomDTO(Room room) {
        this.id = String.valueOf(room.getId());
        this.name = room.getName();
        this.status = new StringUtil().convertIntStatusToString(room.getStatus());
        this.createdAt = new StringUtil().convertMilToString(room.getCreatedAt());
        this.updatedAt = new StringUtil().convertMilToString(room.getUpdatedAt());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
