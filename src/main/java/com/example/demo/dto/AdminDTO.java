package com.example.demo.dto;

import com.example.demo.entity.Admin;
import com.example.demo.util.StringUtil;

public class AdminDTO {
    private String id;
    private String fullName;
    private String phone;
    private String dob;
    private String address;
    private String status;

    public AdminDTO() {
    }

    public AdminDTO(Admin admin) {
        this.id = String.valueOf(admin.getId());
        this.fullName = admin.getFullName();
        this.phone = admin.getPhone();
        this.dob = admin.getDob();
        this.address = admin.getAddress();
        this.status = new StringUtil().convertIntStatusToString(admin.getStatus());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
