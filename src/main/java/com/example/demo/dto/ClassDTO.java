package com.example.demo.dto;

import com.example.demo.entity.Clazz;
import com.example.demo.entity.Room;
import com.example.demo.util.StringUtil;

public class ClassDTO {
    private String id;
    private String name;
    private String roomName;
    private String room_id;
    private String createdAt;
    private String updatedAt;
    private String deletedAt;
    private String status;

    public ClassDTO() {
    }

    public ClassDTO(Clazz clazz) {
        this.id = String.valueOf(clazz.getId());
        this.name = clazz.getName();
        this.room_id = String.valueOf(clazz.getRooms().getId());
        this.roomName = clazz.getRooms().getName();
        this.createdAt = new StringUtil().convertMilToString(clazz.getCreatedAt());
        this.updatedAt = new StringUtil().convertMilToString(clazz.getUpdatedAt());
        this.status = new StringUtil().convertIntStatusToString(clazz.getStatus());
    }

    public ClassDTO(Clazz clazz, Room room) {
        this.id = String.valueOf(clazz.getId());
        this.name = clazz.getName();
        this.createdAt = new StringUtil().convertMilToString(clazz.getCreatedAt());
        this.updatedAt = new StringUtil().convertMilToString(clazz.getUpdatedAt());
        this.status = new StringUtil().convertIntStatusToString(clazz.getStatus());
        this.room_id = String.valueOf(room.getId());
        this.roomName = room.getName();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }
}
