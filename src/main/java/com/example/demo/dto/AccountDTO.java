package com.example.demo.dto;

import com.example.demo.entity.Account;
import com.example.demo.util.StringUtil;

public class AccountDTO {
    private String id;
    private String username;
    private String password;
    private String email;
    private String avatar;
    private String createdAt;
    private String updatedAt;
    private String deletedAt;
    private String status;
    private String role;

    public AccountDTO() {
    }

    public AccountDTO(Account account) {
        this.id = String.valueOf(account.getId());
        this.username = account.getUsername();
        this.email = account.getEmail();
        this.avatar = account.getAvatar();
        this.createdAt = new StringUtil().convertMilToString(account.getCreatedAt());
        this.updatedAt = new StringUtil().convertMilToString(account.getUpdatedAt());
        this.status = new StringUtil().convertIntStatusToString(account.getStatus());
        this.role = new StringUtil().convertIntRoleToString(account.getRole());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
