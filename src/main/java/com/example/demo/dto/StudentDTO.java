package com.example.demo.dto;

import com.example.demo.entity.Student;
import com.example.demo.util.StringUtil;

public class StudentDTO {
    private String id;
    private String fullName;
    private String phone;
    private String dob;
    private String address;
    private String image;
    private String parentPhone;
    private String idCard;
    private String gender;
    private String rollNumber;
    private String status;
    private String startYear;
    private String endYear;

    public StudentDTO() {
    }

    public StudentDTO(Student student){
        this.id = String.valueOf(student.getId());
        this.fullName = student.getFullName();
        this.phone = student.getPhone();
        this.dob = student.getDob();
        this.address = student.getAddress();
        this.image = student.getImage();
        this.parentPhone = student.getParentPhone();
        this.idCard = student.getIdCard();
        this.gender = new StringUtil().convertIntGenderToString(student.getGender());
        this.rollNumber = student.getRollNumber();
        this.status = new StringUtil().convertIntStatusToString(student.getStatus());
        this.startYear = student.getStartYear();
        this.endYear = student.getEndYear();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getParentPhone() {
        return parentPhone;
    }

    public void setParentPhone(String parentPhone) {
        this.parentPhone = parentPhone;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStartYear() {
        return startYear;
    }

    public void setStartYear(String startYear) {
        this.startYear = startYear;
    }

    public String getEndYear() {
        return endYear;
    }

    public void setEndYear(String endYear) {
        this.endYear = endYear;
    }
}
