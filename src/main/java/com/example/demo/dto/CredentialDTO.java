package com.example.demo.dto;

import com.example.demo.entity.Credential;
import com.example.demo.util.StringUtil;

public class CredentialDTO {
    private String accessToken;
    private String tokenExpiredAt;
    private String createdAt;

    public CredentialDTO(Credential credential) {
        this.accessToken = credential.getAccessToken();
        this.tokenExpiredAt = new StringUtil().convertMilToString(credential.getTokenExpiredAt());
        this.createdAt = new StringUtil().convertMilToString(credential.getCreatedAt());
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenExpiredAt() {
        return tokenExpiredAt;
    }

    public void setTokenExpiredAt(String tokenExpiredAt) {
        this.tokenExpiredAt = tokenExpiredAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
