package com.example.demo.dto;


import com.example.demo.entity.Teacher;
import com.example.demo.util.StringUtil;

public class TeacherDTO {
    private String id;
    private String fullName;
    private String phone;
    private String dob;
    private String image;
    private String address;
    private String idCard;
    private String gender;
    private String status;

    public TeacherDTO() {
    }

    public TeacherDTO(Teacher teacher) {
        this.id = String.valueOf(teacher.getId());
        this.fullName = teacher.getFullName();
        this.phone = teacher.getPhone();
        this.dob = teacher.getDob();
        this.image = teacher.getImage();
        this.address = teacher.getAddress();
        this.idCard = teacher.getIdCard();
        this.gender = new StringUtil().convertIntGenderToString(teacher.getGender());
        this.status = new StringUtil().convertIntStatusToString(teacher.getStatus());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
