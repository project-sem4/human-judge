package com.example.demo.dto;

import com.example.demo.entity.Note;
import com.example.demo.entity.Student;
import com.example.demo.entity.Teacher;
import com.example.demo.util.StringUtil;

public class NoteDTO {
    private String id;
    private String note;
    private String nameStudent;
    private String nameTeacher;
    private String status;
    private String createdAt;
    private String updatedAt;
    private String deletedAt;

    public NoteDTO() {
    }

    public NoteDTO(Note note) {
        this.id = String.valueOf(note.getId());
        this.note = note.getNote();
        this.status = new StringUtil().convertIntStatusToString(note.getStatus());
        this.createdAt = new StringUtil().convertMilToString(note.getCreatedAt());
        this.updatedAt = new StringUtil().convertMilToString(note.getUpdatedAt());
    }

    public NoteDTO(Note note, Student student, Teacher teacher) {
        this.id = String.valueOf(note.getId());
        this.note = note.getNote();
        this.status = new StringUtil().convertIntStatusToString(note.getStatus());
        this.createdAt = new StringUtil().convertMilToString(note.getCreatedAt());
        this.updatedAt = new StringUtil().convertMilToString(note.getUpdatedAt());
        this.nameStudent = student.getFullName();
        this.nameTeacher = teacher.getFullName();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getNameStudent() {
        return nameStudent;
    }

    public void setNameStudent(String nameStudent) {
        this.nameStudent = nameStudent;
    }

    public String getNameTeacher() {
        return nameTeacher;
    }

    public void setNameTeacher(String nameTeacher) {
        this.nameTeacher = nameTeacher;
    }
}
