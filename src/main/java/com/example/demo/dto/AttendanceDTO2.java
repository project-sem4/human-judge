package com.example.demo.dto;

import com.example.demo.entity.Attendance;
import com.example.demo.util.StringUtil;

public class AttendanceDTO2 {
    private String id;
    private String studentId;
    private String studentName;
    private String status;
    private String createdAt;
    private String updatedAt;
    private String deletedAt;

    public AttendanceDTO2() {
    }

    public AttendanceDTO2(Attendance attendance) {
        this.id = String.valueOf(attendance.getId());
        this.studentId = String.valueOf(attendance.getStudent().getId());
        this.studentName = attendance.getStudent().getFullName();
        this.status = new StringUtil().convertIntStatusToString(attendance.getStatus());
        this.createdAt = new StringUtil().convertMilToString(attendance.getCreatedAt());
        this.updatedAt = new StringUtil().convertMilToString(attendance.getUpdatedAt());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }
}
