package com.example.demo.dto;

import com.example.demo.entity.Semester;
import com.example.demo.entity.Subject;
import com.example.demo.util.StringUtil;

public class SubjectDTO {
    private String id;
    private String subject;
    private String code;
    private String period;
    private String semesterId;
    private String nameSemester;
    private String status;
    private String createdAt;
    private String updatedAt;
    private String deletedAt;

    public SubjectDTO() {
    }

    public SubjectDTO(Subject subject) {
        this.id = String.valueOf(subject.getId());
        this.subject = subject.getSubject();
        this.period = String.valueOf(subject.getPeriod());
        this.code = subject.getCode();
        this.status = new StringUtil().convertIntStatusToString(subject.getStatus());
        this.createdAt = new StringUtil().convertMilToString(subject.getCreatedAt());
        this.updatedAt = new StringUtil().convertMilToString(subject.getUpdatedAt());

    }
    public SubjectDTO(Subject subject, Semester semester) {
        this.id = String.valueOf(subject.getId());
        this.subject = subject.getSubject();
        this.period = String.valueOf(subject.getPeriod());
        this.code = subject.getCode();
        this.status = new StringUtil().convertIntStatusToString(subject.getStatus());
        this.createdAt = new StringUtil().convertMilToString(subject.getCreatedAt());
        this.updatedAt = new StringUtil().convertMilToString(subject.getUpdatedAt());
        this.nameSemester = semester.getName();
        this.semesterId = String.valueOf(semester.getId());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getNameSemester() {
        return nameSemester;
    }

    public void setNameSemester(String nameSemester) {
        this.nameSemester = nameSemester;
    }

    public String getSemesterId() {
        return semesterId;
    }

    public void setSemesterId(String semesterId) {
        this.semesterId = semesterId;
    }
}
