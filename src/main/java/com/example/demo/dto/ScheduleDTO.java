package com.example.demo.dto;

import com.example.demo.entity.Schedule;
import com.example.demo.util.StringUtil;

public class ScheduleDTO {
    private String id;
    private String clazzId;
    private String clazzName;
    private String subjectId;
    private String subjectName;
    private String roomId;
    private String roomName;
    private String slotId;
    private String slotName;
    private String employeeId;
    private String employeeName;
    private String dayTypeId;
    private String dayTypeName;
    private String teacherId;
    private String teacherName;
    private String status;
    private String date;
    private String startTime;
    private String endTime;

    public ScheduleDTO() {
    }

    public ScheduleDTO(Schedule schedule) {
        this.id = String.valueOf(schedule.getId());
        this.clazzId = String.valueOf(schedule.getClazz().getId());
        this.clazzName = schedule.getClazz().getName();
        this.subjectId = String.valueOf(schedule.getSubject().getId());
        this.subjectName = schedule.getSubject().getSubject();
        this.roomId = String.valueOf(schedule.getRoom().getId());
        this.roomName = schedule.getRoom().getName();
        this.slotId = String.valueOf(schedule.getSlot().getId());
        this.slotName = schedule.getSlot().getName();
        this.employeeId = String.valueOf(schedule.getEmployee().getId());
        this.employeeName = schedule.getEmployee().getFullName();
        this.dayTypeId = String.valueOf(schedule.getDayType().getId());
        this.dayTypeName = schedule.getDayType().getName();
        this.teacherId = String.valueOf(schedule.getTeacher().getId());
        this.teacherName = schedule.getTeacher().getFullName();
        this.date = schedule.getDate();
        this.status = new StringUtil().convertIntStatusToString(schedule.getStatus());
        this.startTime = schedule.getDate() + " " + schedule.getSlot().getStartTime();
        this.endTime = schedule.getDate() + " " + schedule.getSlot().getEndTime();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClazzId() {
        return clazzId;
    }

    public void setClazzId(String clazzId) {
        this.clazzId = clazzId;
    }

    public String getClazzName() {
        return clazzName;
    }

    public void setClazzName(String clazzName) {
        this.clazzName = clazzName;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getSlotId() {
        return slotId;
    }

    public void setSlotId(String slotId) {
        this.slotId = slotId;
    }

    public String getSlotName() {
        return slotName;
    }

    public void setSlotName(String slotName) {
        this.slotName = slotName;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }


    public String getDayTypeId() {
        return dayTypeId;
    }

    public void setDayTypeId(String dayTypeId) {
        this.dayTypeId = dayTypeId;
    }

    public String getDayTypeName() {
        return dayTypeName;
    }

    public void setDayTypeName(String dayTypeName) {
        this.dayTypeName = dayTypeName;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
}
