package com.example.demo.dto;

import com.example.demo.entity.Category;
import com.example.demo.util.StringUtil;

public class CategoryDTO {
    private String id;
    private String name;
    private String adminId;
    private String adminName;
    private String status;
    private String createdAt;
    private String updatedAt;
    private String deletedAt;

    public CategoryDTO() {
    }

    public CategoryDTO(Category category) {
        this.id = String.valueOf(category.getId());
        this.name = category.getName();
        this.adminId = String.valueOf(category.getAdmin().getId());
        this.adminName = category.getAdmin().getFullName();
        this.status = new StringUtil().convertIntStatusToString(category.getStatus());
        this.createdAt = new StringUtil().convertMilToString(category.getCreatedAt());
        this.updatedAt = new StringUtil().convertMilToString(category.getUpdatedAt());
        this.deletedAt = new StringUtil().convertMilToString(category.getDeletedAt());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }
}
