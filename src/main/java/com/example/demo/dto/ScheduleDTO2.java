package com.example.demo.dto;

import com.example.demo.entity.Schedule;
import com.example.demo.util.StringUtil;

public class ScheduleDTO2 {
    private String id;
    private String classId;
    private String start_date;
    private String end_date;
    private String text;
    private String status;
    private String color;

    public ScheduleDTO2() {
    }

    public ScheduleDTO2(Schedule schedule) {
        this.id = String.valueOf(schedule.getId());
        this.start_date = schedule.getDate() + " " + schedule.getSlot().getStartTime();
        this.end_date = schedule.getDate() + " " + schedule.getSlot().getEndTime();
        this.text = schedule.getClazz().getName() + " - " + schedule.getSubject().getSubject();
        this.classId = String.valueOf(schedule.getClazz().getId());
        this.status = new StringUtil().convertIntStatusToString(schedule.getStatus());
        this.color = new StringUtil().getSchedulerColorByStatus(new StringUtil().convertIntStatusToString(schedule.getStatus()));
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }
}
