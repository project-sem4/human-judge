package com.example.demo.util;

import com.example.demo.dto.*;
import com.example.demo.entity.*;
import com.example.demo.service.*;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

@Component
public class SampleDatabase {

    @Autowired
    ClazzService clazzService;

    @Autowired
    SemesterService semesterService;

    @Autowired
    SlotService slotService;

    @Autowired
    AccountService accountService;

    @Autowired
    StudentService studentService;

    @Autowired
    AdminService adminService;

    @Autowired
    EmployeeService employeeService;

    @Autowired
    TeacherService teacherService;

    @Autowired
    DayTypeService dayTypeService;

    @Autowired
    RoomService roomService;

    @Autowired
    NoteService noteService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    NewService newService;

    @Autowired
    SubjectService subjectService;

    Gson gson = new Gson();
    StringUtil stringUtil = new StringUtil();

    public String addSampleAdmin(String name) {
        String fileName = "src\\main\\java\\com\\example\\demo\\json\\" + name + ".json";

        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(new JSONTokener(new InputStreamReader(new FileInputStream(fileName), StandardCharsets.UTF_8)));
            switch (name) {
                case "admin":
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        AccountDTO accountDTO = gson.fromJson(jsonObject.getJSONObject("accountDTO").toString(), AccountDTO.class);
                        AdminDTO adminDTO = gson.fromJson(jsonObject.getJSONObject("adminDTO").toString(), AdminDTO.class);
                        Account account = new Account();
                        account.setUsername(accountDTO.getUsername());
                        account.setPassword(BCrypt.hashpw(accountDTO.getPassword(), BCrypt.gensalt()));
                        account.setEmail(accountDTO.getEmail());
                        account.setRole(Account.Role.ADMIN);
                        Admin admin = new Admin();
                        admin.setFullName(adminDTO.getFullName());
                        admin.setPhone(adminDTO.getPhone());
                        admin.setDob(adminDTO.getDob());
                        admin.setAddress(adminDTO.getAddress());
                        admin.setAccount(account);
                        accountService.register(account);
                        adminService.create(admin);
                    }
                    break;
                case "employee":
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        AccountDTO accountDTO = gson.fromJson(jsonObject.getJSONObject("accountDTO").toString(), AccountDTO.class);
                        EmployeeDTO employeeDTO = gson.fromJson(jsonObject.getJSONObject("employeeDTO").toString(), EmployeeDTO.class);
                        Account account = new Account();
                        account.setUsername(accountDTO.getUsername());
                        account.setPassword(BCrypt.hashpw(accountDTO.getPassword(), BCrypt.gensalt()));
                        account.setEmail(accountDTO.getEmail());
                        account.setRole(Account.Role.MANAGER);
                        Employee employee = new Employee();
                        employee.setFullName(employeeDTO.getFullName());
                        employee.setPhone(employeeDTO.getPhone());
                        employee.setDob(employeeDTO.getDob());
                        employee.setImage(employeeDTO.getImage());
                        employee.setAddress(employeeDTO.getAddress());
                        employee.setIdCard(employeeDTO.getIdCard());
                        employee.setGender(Employee.Gender.findByValue(stringUtil.convertStringGenderToInt(employeeDTO.getGender())));
                        employee.setAccount(account);
                        accountService.register(account);
                        employeeService.create(employee);
                    }
                    break;
                case "student":
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        AccountDTO accountDTO = gson.fromJson(jsonObject.getJSONObject("accountDTO").toString(), AccountDTO.class);
                        StudentDTO studentDTO = gson.fromJson(jsonObject.getJSONObject("studentDTO").toString(), StudentDTO.class);
                        Account account = new Account();
                        account.setUsername(accountDTO.getUsername());
                        account.setPassword(BCrypt.hashpw(accountDTO.getPassword(), BCrypt.gensalt()));
                        account.setEmail(accountDTO.getEmail());
                        account.setRole(Account.Role.STUDENT);
                        Student student = new Student();
                        student.setPhone(studentDTO.getPhone());
                        student.setFullName(studentDTO.getFullName());
                        student.setDob(studentDTO.getDob());
                        student.setAddress(studentDTO.getAddress());
                        student.setImage(studentDTO.getImage());
                        student.setParentPhone(studentDTO.getParentPhone());
                        student.setIdCard(studentDTO.getIdCard());
                        student.setRollNumber(studentDTO.getRollNumber());
                        student.setStartYear(studentDTO.getStartYear());
                        student.setEndYear(studentDTO.getEndYear());
                        student.setGender(Student.Gender.findByValue(stringUtil.convertStringGenderToInt(studentDTO.getGender())));
                        student.setAccount(account);
                        accountService.register(account);
                        studentService.create(student);
                    }
                    break;
                case "teacher":
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        AccountDTO accountDTO = gson.fromJson(jsonObject.getJSONObject("accountDTO").toString(), AccountDTO.class);
                        TeacherDTO teacherDTO = gson.fromJson(jsonObject.getJSONObject("teacherDTO").toString(), TeacherDTO.class);
                        Account account = new Account();
                        account.setUsername(accountDTO.getUsername());
                        account.setPassword(BCrypt.hashpw(accountDTO.getPassword(), BCrypt.gensalt()));
                        account.setEmail(accountDTO.getEmail());
                        account.setRole(Account.Role.TEACHER);
                        Teacher teacher = new Teacher();
                        teacher.setFullName(teacherDTO.getFullName());
                        teacher.setPhone(teacherDTO.getPhone());
                        teacher.setDob(teacherDTO.getDob());
                        teacher.setGender(Teacher.Gender.findByValue(stringUtil.convertStringGenderToInt(teacherDTO.getGender())));
                        teacher.setAddress(teacherDTO.getAddress());
                        teacher.setIdCard(teacherDTO.getIdCard());
                        teacher.setImage(teacherDTO.getImage());
                        teacher.setAccount(account);
                        accountService.register(account);
                        teacherService.create(teacher);
                    }
                    break;
                case "slot":
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String jName = jsonObject.getString("name");
                        String jStartTime = jsonObject.getString("startTime");
                        String jEndTime = jsonObject.getString("endTime");
                        Slot slot = new Slot();
                        slot.setName(jName);
                        slot.setStartTime(jStartTime);
                        slot.setEndTime(jEndTime);
                        slotService.create(slot);
                    }
                    break;
                case "semester":
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String jName = jsonObject.getString("name");
                        Semester semester = new Semester();
                        semester.setName(jName);
                        semesterService.create(semester);
                    }
                    break;
                case "class":
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String jName = jsonObject.getString("name");
                        String jRoomId = jsonObject.getString("roomId");
                        Clazz clazz = new Clazz();
                        clazz.setName(jName);
                        clazz.setRooms(roomService.findByRoomId(Long.parseLong(jRoomId)));
                        clazzService.create(clazz);
                    }
                    break;
                case "day":
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String jName = jsonObject.getString("name");
                        DayType dayType = new DayType();
                        dayType.setName(jName);
                        dayTypeService.create(dayType);
                    }
                    break;
                case "room":
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String jName = jsonObject.getString("name");
                        Room room = new Room();
                        room.setName(jName);
                        roomService.create(room);
                    }
                    break;
                case "note":
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String jNote = jsonObject.getString("note");
                        String jStudentId = jsonObject.getString("studentId");
                        String jTeacherId = jsonObject.getString("teacherId");
                        Note note = new Note();
                        note.setStudent(studentService.findByStudentId(Long.parseLong(jStudentId)));
                        note.setTeacher(teacherService.findByTeacherId(Long.parseLong(jTeacherId)));
                        note.setNote(jNote);
                        noteService.create(note);
                    }
                    break;
                case "category":
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String jName = jsonObject.getString("name");
                        String jAdminId = jsonObject.getString("adminId");
                        Category category = new Category();
                        category.setName(jName);
                        category.setAdmin(adminService.findByAdminId(Long.parseLong(jAdminId)));
                        categoryService.create(category);
                    }
                    break;
                case "article":
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String jTitle = jsonObject.getString("title");
                        String jDescription = jsonObject.getString("description");
                        String jContent = jsonObject.getString("content");
                        String jAdminId = jsonObject.getString("adminId");
                        String jCategoryId = jsonObject.getString("categoryId");
                        Article article = new Article();
                        article.setTitle(jTitle);
                        article.setDescription(jDescription);
                        article.setContent(jContent);
                        article.setAdmin(adminService.findByAdminId(Long.parseLong(jAdminId)));
                        article.setCategory(categoryService.findByCategoryId(Long.parseLong(jCategoryId)));
                        newService.create(article);
                    }
                    break;
                case "subject":
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String jCode = jsonObject.getString("code");
                        String jSubject = jsonObject.getString("subject");
                        String jPeriod = jsonObject.getString("period");
                        String jSemesterId = jsonObject.getString("semesterId");
                        Subject subject = new Subject();
                        subject.setCode(jCode);
                        subject.setSubject(jSubject);
                        subject.setPeriod(Integer.parseInt(jPeriod));
                        subject.setSemester(semesterService.findBySemesterId(Long.parseLong(jSemesterId)));
                        subjectService.create(subject);
                    }
                    break;
            }
            return "OK";
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "Failed";
        }
    }
}
