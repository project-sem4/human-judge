package com.example.demo.util;

import java.text.DateFormat;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Pattern;

public class StringUtil {
    private Random random = new Random();

    public int genDigits() {
        return 10000000 + random.nextInt(90000000);
    }

    public String convertStringDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        return sdf.format(now);
    }

    public String convertMilToString(long timeSTamp) {
        Date date = new Date(timeSTamp);
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        return formatter.format(date);
    }

    public String getSchedulerColorByStatus(String status) {
        String color;
        switch (status) {
            case "Pending":
                color = "#ff0000";
                break;
            case "Active":
                color = "#00c10d";
                break;
            case "Done":
                color = "#ff5a00";
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + status);
        }
        return color;
    }

    public String convertIntStatusToString(int i) {
        String stt = "";
        if (i == -1) {
            stt = "Deleted";
        } else if (i == 0) {
            stt = "DeActive";
        } else if (i == 1) {
            stt = "Active";
        } else if (i == 2) {
            stt = "Studying";
        } else if (i == 3) {
            stt = "Graduated";
        } else if (i == 4) {
            stt = "Reserve";
        } else if (i == 5) {
            stt = "Waiting Class";
        } else if (i == 6) {
            stt = "Working";
        } else if (i == 7) {
            stt = "Retired";
        } else if (i == 8) {
            stt = "Openning";
        } else if (i == 9) {
            stt = "Closed";
        } else if (i == 10) {
            stt = "Enable";
        } else if (i == 11) {
            stt = "Disable";
        } else if (i == 12) {
            stt = "Absent";
        } else if (i == 13) {
            stt = "Attended";
        } else if (i == 14) {
            stt = "Done";
        } else if (i == 15) {
            stt = "Pending";
        } else if (i == 16) {
            stt = "Available";
        } else if (i == 17) {
            stt = "Unavailable";
        }
        return stt;
    }

    public int convertStringStatusToInt(String status) {
        int intSt = 0;
        switch (status) {
            case "Deleted":
                intSt = -1;
                break;
            case "DeActive":
                intSt = 0;
                break;
            case "Active":
                intSt = 1;
                break;
            case "Studying":
                intSt = 2;
                break;
            case "Graduated":
                intSt = 3;
                break;
            case "Reserve":
                intSt = 4;
                break;
            case "Waiting Class":
                intSt = 5;
                break;
            case "Working":
                intSt = 6;
                break;
            case "Retired":
                intSt = 7;
                break;
            case "Openning":
                intSt = 8;
                break;
            case "Closed":
                intSt = 9;
                break;
            case "Enable":
                intSt = 10;
                break;
            case "Disable":
                intSt = 11;
                break;
            case "Absent":
                intSt = 12;
                break;
            case "Attended":
                intSt = 13;
                break;
            case "Done":
                intSt = 14;
                break;
            case "Pending":
                intSt = 15;
                break;
            case "Available":
                intSt = 16;
                break;
            case "Unavailable":
                intSt = 17;
                break;
        }
        return intSt;
    }

    public String convertIntRoleToString(int i) {
        String role = "";
        if (i == 1) {
            role = "Student";
        } else if (i == 2) {
            role = "Admin";
        } else if (i == 3) {
            role = "Manager";
        } else if (i == 4) {
            role = "Teacher";
        }
        return role;
    }

    public int convertStringRoleToInt(String role) {
        int i = 0;
        switch (role) {
            case "Student":
                i = 1;
                break;
            case "Admin":
                i = 2;
                break;
            case "Manager":
                i = 3;
                break;
            case "Teacher":
                i = 4;
                break;
        }
        return i;
    }

    public String convertIntGenderToString(int i) {
        String gen = "";
        if (i == 0) {
            gen = "Female";
        } else if (i == 1) {
            gen = "Male";
        } else if (i == 2) {
            gen = "Others";
        }
        return gen;
    }

    public int convertStringGenderToInt(String gender) {
        int gen = 0;
        switch (gender) {
            case "Male":
                gen = 1;
                break;
            case "Female":
                gen = 0;
                break;
            case "Others":
                gen = 2;
                break;
        }
        return gen;
    }

    public String generateUsernameFromFullName(String fullName) {
        String[] words = deAccent(fullName).split(" ");
        String lastword = words[words.length - 1];
        StringBuilder splitChar = new StringBuilder();
        for (int i = 0; i < words.length - 1; i++) {
            splitChar.append(words[i].charAt(0));
        }
        return (lastword + splitChar).toLowerCase();
    }

    public String deAccent(String str) {
        try {
            String temp = Normalizer.normalize(str, Normalizer.Form.NFD);
            Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
            return pattern.matcher(temp).replaceAll("").toLowerCase().replaceAll("đ", "d");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    public List<String> autoGenerateSchedule(String startDate, String endDate, int period, String dayType) {
        LocalDate startDate1 = LocalDate.parse(startDate);
        LocalDate endDate1 = LocalDate.parse(endDate);
        List<LocalDate> totalDates = new ArrayList<>();
        List<String> filterTotalDates = new ArrayList<>();
        String[] splitDayType = dayType.split("-");

        while (!startDate1.isAfter(endDate1)) {
            totalDates.add(startDate1);
            startDate1 = startDate1.plusDays(1);
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        for (int i = 0; i <= totalDates.size(); i++) {
            if (!startDate1.getDayOfWeek().name().equals(DayOfWeek.SUNDAY) && totalDates.get(i).getDayOfWeek().getValue() == (Integer.parseInt(splitDayType[0].trim()) - 1)
                    || totalDates.get(i).getDayOfWeek().getValue() == (Integer.parseInt(splitDayType[1].trim()) - 1)
                    || totalDates.get(i).getDayOfWeek().getValue() == (Integer.parseInt(splitDayType[2].trim()) - 1)) {
                String formattedString = totalDates.get(i).format(formatter);
                System.out.println(formattedString);
                filterTotalDates.add(formattedString);
                if (filterTotalDates.size() > period - 1) {
                    break;
                }
            }
        }
        return filterTotalDates;
    }

    public int convertCounterScheduleandCounterAttendancetoPercent(int counterSchedule, int counterAttendance) {
        int n;
        if (counterAttendance == 0 || counterSchedule == 0) {
            return 0;
        }
        n = (counterAttendance / counterSchedule) * 100;
        return n;
    }

    public static void main(String[] args) {
        StringUtil stringUtil = new StringUtil();
        System.out.println(stringUtil.generateUsernameFromFullName("Đào Duy Khánh"));
        new StringUtil().autoGenerateSchedule("2019-11-10", "2019-12-10", 10, "2 - 4 - 6");
    }
}
