package com.example.demo.endpoint;

import com.example.demo.dto.AttendanceDTO;
import com.example.demo.dto.AttendanceDTO2;
import com.example.demo.dto.context.AttendanceScheduleContext;
import com.example.demo.entity.Attendance;
import com.example.demo.entity.Schedule;
import com.example.demo.entity.Student;
import com.example.demo.service.*;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import com.example.demo.util.JsonResponse;
import com.example.demo.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Logger;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/_api/v1/attendances")
public class AttendanceEndpoint {

    private static Logger LOGGER = Logger.getLogger(AttendanceEndpoint.class.getSimpleName());

    @Autowired
    AttendanceService attendanceService;
    @Autowired
    StudentService studentService;
    @Autowired
    SubjectService subjectService;
    @Autowired
    TeacherService teacherService;
    @Autowired
    ClazzService clazzService;
    @Autowired
    SemesterService semesterService;
    @Autowired
    SlotService slotService;
    @Autowired
    ScheduleService scheduleService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Object> getList(
            @RequestParam(value = "keyword", required = false) String keyword,
            @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to) {
        Specification specification = Specification.where(null);
        if (keyword != null && keyword.length() > 0) {
            specification = specification
                    .and(new ObjectSpecification(new SearchCriteria("time", ":", keyword)));
        }
        List<Attendance> attendances = attendanceService.getAttendances(specification);
        Set<AttendanceDTO> mySet = new HashSet<>();
        for (Attendance attendance: attendances) {
            mySet.add(new AttendanceDTO(attendance));
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Action success!")
                .setData(mySet),
                HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<Object> getDetail(@PathVariable String id) {
        Attendance attendance = attendanceService.findByAttendanceId(Long.parseLong(id));
        if (attendance == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new AttendanceDTO(attendanceService.findByAttendanceId(Long.parseLong(id)))),
                HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<Object> update(@Valid @PathVariable String id, @RequestBody AttendanceDTO updateAttendanceDTO) {
        Attendance existAttendance = attendanceService.findByAttendanceId(Long.parseLong(id));
        if (existAttendance == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        existAttendance.setStatus(Attendance.Status.findByValue(new StringUtil().convertStringStatusToInt(updateAttendanceDTO.getStatus())));
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new AttendanceDTO(attendanceService.update(existAttendance))),
                HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAttendanceByStudent")
    public ResponseEntity<Object> getAttendanceByStudent(@RequestParam(value = "studentId", required = false) String studentId) {
        List<Attendance> attendances = attendanceService.findAttendancesByStudent(Long.parseLong(studentId));
        if (attendances.size() == 0) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        Set<AttendanceDTO> mySet = new HashSet<>();
        for (Attendance attendance : attendances) {
            mySet.add(new AttendanceDTO(attendance));
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(mySet),
                HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<Object> delete(@PathVariable String id) {
        Attendance deleteAttendance = attendanceService.findByAttendanceId(Long.parseLong(id));
        if (deleteAttendance == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        attendanceService.delete(deleteAttendance);
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Simple Success"),
                HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAttendanceBySchedule")
    public ResponseEntity<Object> getAttendanceBySchedule(@RequestParam(value = "scheduleId", required = false) String scheduleId) {
        List<Attendance> attendances = attendanceService.findBySchedule(Long.parseLong(scheduleId));
        if (attendances.size() == 0) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }

        Set<AttendanceDTO2> mySet = new HashSet<>();
        for (Attendance attendance: attendances) {
            mySet.add(new AttendanceDTO2(attendance));
        }

        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Action success!")
                .setData(new AttendanceScheduleContext(attendances.get(0), mySet)),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getAttendanceByStudentAndSchedule")
    public ResponseEntity<Object> getAttendanceByStudentAndSchedule(@RequestParam(value = "scheduleId", required = false) String scheduleId,
                                                                    @RequestParam(value = "studentId", required =  false) String studentId) {
        Schedule schedule = scheduleService.findByScheduleId(Long.parseLong(scheduleId));
        if (schedule == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }

        Student student = studentService.findByStudentId(Long.parseLong(studentId));
        if (student == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        Attendance attendances = attendanceService.findByScheduleIdAndStudentId(schedule.getId(), student.getId());
        if (attendances == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Action success!")
                .setData(new AttendanceDTO(attendances)),
                HttpStatus.OK);
    }
}

