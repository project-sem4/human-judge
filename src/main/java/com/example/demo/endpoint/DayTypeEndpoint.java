package com.example.demo.endpoint;

import com.example.demo.dto.DayTypeDTO;
import com.example.demo.entity.DayType;
import com.example.demo.service.DayTypeService;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import com.example.demo.util.JsonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/_api/v1/dayType")
public class DayTypeEndpoint {
    private static Logger LOGGER = Logger.getLogger(ArticleEndpoint.class.getSimpleName());
    @Autowired
    DayTypeService dayTypeService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Object> getList(
            @RequestParam(value = "keyword", required = false) String keyword,
            @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to) {
        Specification specification = Specification.where(null);
        if (keyword != null && keyword.length() > 0) {
            specification = specification
                    .and(new ObjectSpecification(new SearchCriteria("name", ":", keyword)));
        }
        List<DayType> dayTypes = dayTypeService.getDayType(specification);
        Set<DayTypeDTO> mySet = new HashSet<>();
        for (DayType dayType : dayTypes) {
            mySet.add(new DayTypeDTO(dayType));
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Action success!")
                .setData(mySet),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Object> store(@Valid @RequestBody DayTypeDTO dayTypeDTO) {
        DayType dayType = new DayType();
        dayType.setName(dayTypeDTO.getName());
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new DayTypeDTO(dayTypeService.create(dayType))),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<Object> getDetail(@PathVariable String id) {
        DayType dayType = dayTypeService.findByDayTypeId(Long.parseLong(id));
        if (dayType == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new DayTypeDTO(dayTypeService.findByDayTypeId(Long.parseLong(id)))),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<Object> update(@Valid @PathVariable String id, @RequestBody DayTypeDTO dayTypeDTO) {
        DayType existDayType = dayTypeService.findByDayTypeId(Long.parseLong(id));
        if (existDayType == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        existDayType.setName(dayTypeDTO.getName());
        existDayType.setUpdatedAt(Calendar.getInstance().getTimeInMillis());
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new DayTypeDTO(dayTypeService.update(existDayType))),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<Object> delete(@PathVariable String id) {
        DayType deleteDayType = dayTypeService.findByDayTypeId(Long.parseLong(id));
        if (deleteDayType == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        dayTypeService.delete(deleteDayType);
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Simple Success"),
                HttpStatus.OK);
    }
}
