package com.example.demo.endpoint;

import com.example.demo.util.SampleDatabase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/addSampleDB")
public class AddSampleDatabaseEndpoint {
    @Autowired
    SampleDatabase sampleDatabase;

    @RequestMapping(method = RequestMethod.GET)
    public void addSampleDB(@RequestParam(value = "name", required = false) String name) {
        sampleDatabase.addSampleAdmin(name);
    }
}
