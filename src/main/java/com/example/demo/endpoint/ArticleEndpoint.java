package com.example.demo.endpoint;

import com.example.demo.dto.ArticleDTO;
import com.example.demo.entity.Admin;
import com.example.demo.entity.Article;
import com.example.demo.entity.Category;
import com.example.demo.service.AdminService;
import com.example.demo.service.CategoryService;
import com.example.demo.service.NewService;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import com.example.demo.util.JsonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;
import java.util.logging.Logger;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/_api/v1/news")
public class ArticleEndpoint {

    private static Logger LOGGER = Logger.getLogger(ArticleEndpoint.class.getSimpleName());

    @Autowired
    NewService newService;
    @Autowired
    CategoryService categoryService;
    @Autowired
    AdminService adminService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Object> getList(
            @RequestParam(value = "keyword", required = false) String keyword,
            @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to) {
        Specification specification = Specification.where(null);
        if (keyword != null && keyword.length() > 0) {
            specification = specification
                    .and(new ObjectSpecification(new SearchCriteria("title", ":", keyword)));
        }
        List<Article> articles = newService.getNews(specification);
        Set<ArticleDTO> mySet = new HashSet<>();
        for (Article article : articles) {
            mySet.add(new ArticleDTO(article));
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Action success!")
                .setData(mySet),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<Object> getDetail(@PathVariable String id) {
        Article articleDetail = newService.findByNewId(Long.parseLong(id));
        if (articleDetail == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new ArticleDTO(newService.findByNewId(Long.parseLong(id)))),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Object> store(@Valid @RequestBody ArticleDTO articleDTO) {
        Admin admin = adminService.findByAdminId(Long.parseLong(articleDTO.getAdminId()));
        Category category = categoryService.findByCategoryId(Long.parseLong(articleDTO.getCategoryId()));
        Article createArticle = new Article();
        createArticle.setTitle(articleDTO.getTitle());
        createArticle.setContent(articleDTO.getContent());
        createArticle.setDescription(articleDTO.getDescription());
        createArticle.setAdmin(admin);
        createArticle.setCategory(category);
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new ArticleDTO(newService.create(createArticle))),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<Object> update(@Valid @PathVariable String id, @RequestBody ArticleDTO updateArticleDTO) {
        Article existArticle = newService.findByNewId(Long.parseLong(id));
        if (existArticle == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        Category category = categoryService.findByCategoryId(Long.parseLong(updateArticleDTO.getCategoryId()));
        if (category == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        existArticle.setTitle(updateArticleDTO.getTitle());
        existArticle.setContent(updateArticleDTO.getContent());
        existArticle.setDescription(updateArticleDTO.getDescription());
        existArticle.setCategory(category);
        existArticle.setUpdatedAt(Calendar.getInstance().getTimeInMillis());
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new ArticleDTO(newService.update(existArticle))),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<Object> delete(@PathVariable String id) {
        Article deleteArticle = newService.findByNewId(Long.parseLong(id));
        if (deleteArticle == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        newService.delete(deleteArticle);
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Simple Success"),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getArticleByCategory")
    public ResponseEntity<Object> getArticleByCategory(@RequestParam(value = "catId", required = false) String catId) {
        Category category = categoryService.findByCategoryId(Long.parseLong(catId));
        if (category == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        Set<ArticleDTO> mySet = new HashSet<>();
        for (Article article : category.getArticles()) {
            mySet.add(new ArticleDTO(article));
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Action success!")
                .setData(mySet),
                HttpStatus.OK);
    }
}
