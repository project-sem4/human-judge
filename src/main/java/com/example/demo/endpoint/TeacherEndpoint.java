package com.example.demo.endpoint;

import com.example.demo.dto.TeacherDTO;
import com.example.demo.entity.Teacher;
import com.example.demo.service.ClazzService;
import com.example.demo.service.TeacherService;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import com.example.demo.util.JsonResponse;
import com.example.demo.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/_api/v1/teachers")
public class TeacherEndpoint {

    private static Logger LOGGER = Logger.getLogger(TeacherEndpoint.class.getSimpleName());

    @Autowired
    TeacherService teacherService;
    @Autowired
    ClazzService clazzService;

    StringUtil stringUtil = new StringUtil();

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Object> getList(
            @RequestParam(value = "keyword", required = false) String keyword,
            @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to) {
        Specification specification = Specification.where(null);
        if (keyword != null && keyword.length() > 0) {
            specification = specification
                    .and(new ObjectSpecification(new SearchCriteria("fullName", ":", keyword)))
                    .or(new ObjectSpecification(new SearchCriteria("idCard", ":", keyword)))
                    .or(new ObjectSpecification(new SearchCriteria("phone", ":", keyword)));
        }
        List<Teacher> teachers = teacherService.getTeachers(specification);
        Set<TeacherDTO> mySet = new HashSet<>();
        for (Teacher teacher: teachers) {
            mySet.add(new TeacherDTO(teacher));
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Action success!")
                .setData(mySet),
                HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<Object> getDetail(@PathVariable String id) {
        Teacher teacher = teacherService.findByTeacherId(Long.parseLong(id));
        if (teacher == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new TeacherDTO(teacherService.findByTeacherId(Long.parseLong(id)))),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<Object> update(@Valid @PathVariable String id, @RequestBody TeacherDTO teacherDTO) {
        Teacher existTeacher = teacherService.findByTeacherId(Long.parseLong(id));
        if (existTeacher == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        existTeacher.setFullName(teacherDTO.getFullName());
        existTeacher.setPhone(teacherDTO.getPhone());
        existTeacher.setDob(teacherDTO.getDob());
        existTeacher.setAddress(teacherDTO.getAddress());
        existTeacher.setIdCard(teacherDTO.getIdCard());
        existTeacher.setGender(Teacher.Gender.findByValue(stringUtil.convertStringGenderToInt(teacherDTO.getGender())));
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new TeacherDTO(teacherService.update(existTeacher))),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<Object> delete(@PathVariable String id) {
        Teacher deleteTeacher = teacherService.findByTeacherId(Long.parseLong(id));
        if (deleteTeacher == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        teacherService.delete(deleteTeacher);
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Simple Success"),
                HttpStatus.OK);
    }
}
