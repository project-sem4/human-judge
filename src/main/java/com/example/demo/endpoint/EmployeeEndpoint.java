package com.example.demo.endpoint;

import com.example.demo.dto.EmployeeDTO;
import com.example.demo.entity.Employee;
import com.example.demo.service.EmployeeService;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import com.example.demo.util.JsonResponse;
import com.example.demo.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/_api/v1/employees")
public class EmployeeEndpoint {

    private static Logger LOGGER = Logger.getLogger(EmployeeEndpoint.class.getSimpleName());

    @Autowired
    EmployeeService employeeService;

    StringUtil stringUtil = new StringUtil();

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Object> getList(
            @RequestParam(value = "keyword", required = false) String keyword,
            @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to) {
        Specification specification = Specification.where(null);
        if (keyword != null && keyword.length() > 0) {
            specification = specification
                    .and(new ObjectSpecification(new SearchCriteria("fullName", ":", keyword)))
                    .or(new ObjectSpecification(new SearchCriteria("idCard", ":", keyword)));
        }
        List<Employee> employees = employeeService.getEmployees(specification);
        Set<EmployeeDTO> mySet = new HashSet<>();
        for (Employee employee : employees) {
            mySet.add(new EmployeeDTO(employee));
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Action success!")
                .setData(mySet),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<Object> getDetail(@PathVariable String id) {
        Employee employee = employeeService.findByEmployeeId(Long.parseLong(id));
        if (employee == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new EmployeeDTO(employeeService.findByEmployeeId(Long.parseLong(id)))),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<Object> update(@Valid @PathVariable String id, @RequestBody EmployeeDTO updateEmployeeDTO) {
        Employee existEmployee = employeeService.findByEmployeeId(Long.parseLong(id));
        if (existEmployee == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        existEmployee.setFullName(updateEmployeeDTO.getFullName());
        existEmployee.setPhone(updateEmployeeDTO.getPhone());
        existEmployee.setDob(updateEmployeeDTO.getDob());
        existEmployee.setAddress(updateEmployeeDTO.getAddress());
        existEmployee.setIdCard(updateEmployeeDTO.getIdCard());
        existEmployee.setGender(Employee.Gender.findByValue(stringUtil.convertStringGenderToInt(updateEmployeeDTO.getGender())));
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new EmployeeDTO(employeeService.update(existEmployee))),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<Object> delete(@PathVariable String id) {
        Employee deleteEmployee = employeeService.findByEmployeeId(Long.parseLong(id));
        if (deleteEmployee == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        employeeService.delete(deleteEmployee);
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Simple Success"),
                HttpStatus.OK);
    }
}
