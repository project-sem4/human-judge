package com.example.demo.endpoint;

import com.example.demo.dto.SemesterDTO;
import com.example.demo.entity.Semester;
import com.example.demo.service.SemesterService;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import com.example.demo.util.JsonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/_api/v1/semesters")
public class SemesterEndpoint {

    private static Logger LOGGER = Logger.getLogger(SemesterEndpoint.class.getSimpleName());

    @Autowired
    SemesterService semesterService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Object> getList(
            @RequestParam(value = "keyword", required = false) String keyword,
            @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to) {
        Specification specification = Specification.where(null);
        if (keyword != null && keyword.length() > 0) {
            specification = specification
                    .and(new ObjectSpecification(new SearchCriteria("name", ":", keyword)));
        }
        List<Semester> semesters = semesterService.getSemesters(specification);
        Set<SemesterDTO> mySet = new HashSet<>();
        for (Semester semester: semesters) {
            mySet.add(new SemesterDTO(semester));
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Action success!")
                .setData(mySet),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Object> store(@Valid @RequestBody SemesterDTO semesterDTO) {
        Semester semester = new Semester();
        semester.setName(semesterDTO.getName());
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new SemesterDTO(semesterService.create(semester))),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<Object> getDetail(@PathVariable String id) {
        Semester semester = semesterService.findBySemesterId(Long.parseLong(id));
        if (semester == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new SemesterDTO(semesterService.findBySemesterId(Long.parseLong(id)))),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<Object> update(@Valid @PathVariable String id, @RequestBody SemesterDTO updateSemesterDTO) {
        Semester existSemester = semesterService.findBySemesterId(Long.parseLong(id));
        if (existSemester == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        existSemester.setName(updateSemesterDTO.getName());
        existSemester.setUpdatedAt(Calendar.getInstance().getTimeInMillis());
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new SemesterDTO(semesterService.update(existSemester))),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<Object> delete(@PathVariable String id) {
        Semester deleteSemester = semesterService.findBySemesterId(Long.parseLong(id));
        if (deleteSemester == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        semesterService.delete(deleteSemester);
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Simple Success"),
                HttpStatus.OK);
    }
}
