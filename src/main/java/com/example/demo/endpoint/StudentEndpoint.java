package com.example.demo.endpoint;

import com.example.demo.dto.ClassDTO;
import com.example.demo.dto.StudentDTO;
import com.example.demo.dto.context.ClazzStudentContext;
import com.example.demo.entity.Clazz;
import com.example.demo.entity.Student;
import com.example.demo.service.ClazzService;
import com.example.demo.service.StudentService;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import com.example.demo.util.JsonResponse;
import com.example.demo.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/_api/v1/students")
public class StudentEndpoint {

    private static Logger LOGGER = Logger.getLogger(StudentEndpoint.class.getSimpleName());

    @Autowired
    StudentService studentService;
    @Autowired
    ClazzService clazzService;

    StringUtil stringUtil = new StringUtil();

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Object> getList(
            @RequestParam(value = "keyword", required = false) String keyword,
            @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to) {
        Specification specification = Specification.where(null);
        if (keyword != null && keyword.length() > 0) {
            specification = specification
                    .and(new ObjectSpecification(new SearchCriteria("fullName", ":", keyword)))
                    .or(new ObjectSpecification(new SearchCriteria("phone", ":", keyword)))
                    .or(new ObjectSpecification(new SearchCriteria("idCard", ":", keyword)))
                    .or(new ObjectSpecification(new SearchCriteria("dob", ":", keyword)))
                    .or(new ObjectSpecification(new SearchCriteria("startYear", ":", keyword)))
                    .or(new ObjectSpecification(new SearchCriteria("rollNumber", ":", keyword)));
        }
        List<Student> students = studentService.getStudents(specification);
        Set<StudentDTO> mySet = new HashSet<>();
        for (Student student : students) {
            mySet.add(new StudentDTO(student));
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Action success!")
                .setData(mySet),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getStudentInClass")
    public ResponseEntity<Object> getStudentInClass(@RequestParam(value = "clazzId", required = false) String clazzId) {
        Clazz clazz = clazzService.findByClazzId(Long.parseLong(clazzId));
        List<StudentDTO> studentDTOS = new ArrayList<>();
        for (Student student : clazz.getStudents()) {
            studentDTOS.add(new StudentDTO(student));
            studentService.studentClass(student);
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new ClazzStudentContext(new ClassDTO(clazz),
                        studentDTOS)),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<Object> getDetail(@PathVariable String id) {
        Student student = studentService.findByStudentId(Long.parseLong(id));
        if (student == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new StudentDTO(studentService.findByStudentId(Long.parseLong(id)))),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<Object> update(@Valid @PathVariable String id, @RequestBody StudentDTO updateStudentDTO) {
        Student existStudent = studentService.findByStudentId(Long.parseLong(id));
        if (existStudent == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        existStudent.setFullName(updateStudentDTO.getFullName());
        existStudent.setPhone(updateStudentDTO.getPhone());
        existStudent.setDob(updateStudentDTO.getDob());
        existStudent.setAddress(updateStudentDTO.getAddress());
        existStudent.setParentPhone(updateStudentDTO.getParentPhone());
        existStudent.setIdCard(updateStudentDTO.getIdCard());
        existStudent.setGender(Student.Gender.findByValue(stringUtil.convertStringGenderToInt(updateStudentDTO.getGender())));
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new StudentDTO(studentService.update(existStudent))),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<Object> delete(@PathVariable String id) {
        Student deleteStudent = studentService.findByStudentId(Long.parseLong(id));
        if (deleteStudent == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        studentService.delete(deleteStudent);
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Simple Success"),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/updateListImageForStudent")
    public ResponseEntity<Object> updateListImageForStudent(@RequestParam(value = "studentId", required = false) String studentId,
                                                            @RequestBody StudentDTO studentDTO) {
        Student student = studentService.findByStudentId(Long.parseLong(studentId));
        if (student == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }

        student.setImage(studentDTO.getImage());
        student.setTrainingStatus(0);

        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new StudentDTO(studentService.update(student))),
                HttpStatus.OK);
    }
}
