package com.example.demo.endpoint;

import com.example.demo.dto.ClassDTO;
import com.example.demo.dto.StudentDTO;
import com.example.demo.dto.context.ClazzStudentContext;
import com.example.demo.dto.context.StudentClazzContext;
import com.example.demo.entity.Clazz;
import com.example.demo.entity.Room;
import com.example.demo.entity.Student;
import com.example.demo.service.*;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import com.example.demo.util.JsonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/_api/v1/clazz")
public class ClazzEndpoint {

    private static Logger LOGGER = Logger.getLogger(ClazzEndpoint.class.getSimpleName());

    @Autowired
    ClazzService clazzService;
    @Autowired
    TeacherService teacherService;
    @Autowired
    StudentService studentService;

    @Autowired
    RoomService roomService;

    @Autowired
    SlotService slotService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Object> getList(
            @RequestParam(value = "keyword", required = false) String keyword,
            @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to) {
        Specification specification = Specification.where(null);
        if (keyword != null && keyword.length() > 0) {
            specification = specification
                    .and(new ObjectSpecification(new SearchCriteria("name", ":", keyword)))
                    .or(new ObjectSpecification(new SearchCriteria("startTime", ":", keyword)));
        }
        List<Clazz> clazzes = clazzService.getClazzs(specification);

        Set<ClassDTO> mySet = new HashSet<>();
        for (Clazz clazz : clazzes) {
            if (clazz.getRooms() == null) {
                clazz.setRooms(new Room());
            }
            mySet.add(new ClassDTO(clazz));
        }

        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Action success!")
                .setData(mySet),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Object> store(@Valid
                                        @RequestParam(value = "roomId", required = false) String roomId,
                                        @RequestBody ClassDTO clazzDTO) {
        Room room = roomService.findByRoomId(Long.parseLong(roomId));
        Clazz clazz = new Clazz();
        clazz.setName(clazzDTO.getName());
        clazz.setRooms(room);

        Clazz clazz1 = clazzService.create(clazz);
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new ClassDTO(clazz1, clazz1.getRooms())),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getClassByStudent")
    public ResponseEntity<Object> getClassByStudent(@RequestParam(value = "studentId", required = false) String studentId) {
        Student student = studentService.findByStudentId(Long.parseLong(studentId));
        List<ClassDTO> clazzDTOS = new ArrayList<>();
        for (Clazz clazz : student.getClasses()) {
            clazzDTOS.add(new ClassDTO(clazz, clazz.getRooms()));
            clazzService.classStudent(clazz);
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new StudentClazzContext(new StudentDTO(student), clazzDTOS)),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/arrangeclass")
    public ResponseEntity<Object> arrangeClass(@RequestParam(value = "studentList", required = false) List<String> studentList, @RequestParam String classId) {
        Clazz clazz = clazzService.findByClazzId(Long.parseLong(classId));
        if (clazz == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        Set<Clazz> clazzes = new HashSet<>();
        clazzes.add(clazz);

        Set<Student> students = new HashSet<>();
        for (int i = 0; i < studentList.size(); i++) {
            Student student = studentService.findByStudentId(Long.parseLong(studentList.get(i).trim()));
            if (student != null) {
                students.add(student);
            } else {
                return new ResponseEntity<>(new JsonResponse()
                        .setStatus(HttpStatus.NOT_FOUND.value())
                        .setMessage("Not found"),
                        HttpStatus.NOT_FOUND);
            }
        }

        for (Clazz clazz1 : clazzes) {
            clazz1.setStudents(students);
        }

        List<StudentDTO> studentDTOS = new ArrayList<>();
        for (Student student : students) {
            student.setClasses(clazzes);
            studentDTOS.add(new StudentDTO(student));
            studentService.studentClass(student);
        }

        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new ClazzStudentContext(new ClassDTO(clazz),
                        studentDTOS)),
                HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<Object> getDetail(@PathVariable String id) {
        Clazz clazz = clazzService.findByClazzId(Long.parseLong(id));
        if (clazz == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new ClassDTO(clazz, clazz.getRooms())),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<Object> update(@Valid @PathVariable String id,
                                         @RequestParam(value = "roomId", required = false) String roomId,
                                         @RequestBody ClassDTO updateClazzDTO) {
        Clazz existClazz = clazzService.findByClazzId(Long.parseLong(id));
        if (existClazz == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        Room room = roomService.findByRoomId(Long.parseLong(roomId));
        System.out.println(room);
        if (room == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        existClazz.setName(updateClazzDTO.getName());
        existClazz.setRooms(room);
        clazzService.update(existClazz);
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new ClassDTO(existClazz, existClazz.getRooms())),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<Object> delete(@PathVariable String id) {
        Clazz deleteClazz = clazzService.findByClazzId(Long.parseLong(id));
        if (deleteClazz == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        clazzService.delete(deleteClazz);
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Simple Success"),
                HttpStatus.OK);
    }
}
