package com.example.demo.endpoint;

import com.example.demo.dto.CategoryDTO;
import com.example.demo.entity.Admin;
import com.example.demo.entity.Category;
import com.example.demo.service.AdminService;
import com.example.demo.service.CategoryService;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import com.example.demo.util.JsonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/_api/v1/category")
public class CategoryEndpoint {

    private static Logger LOGGER = Logger.getLogger(CategoryEndpoint.class.getSimpleName());

    @Autowired
    CategoryService categoryService;
    @Autowired
    AdminService adminService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Object> getList(
            @RequestParam(value = "keyword", required = false) String keyword,
            @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to) {
        Specification specification = Specification.where(null);
        if (keyword != null && keyword.length() > 0) {
            specification = specification
                    .and(new ObjectSpecification(new SearchCriteria("name", ":", keyword)));
        }
        List<Category> categories = categoryService.getCategories(specification);
        Set<CategoryDTO> mySet = new HashSet<>();
        for (Category category: categories) {
            mySet.add(new CategoryDTO(category));
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Action success!")
                .setData(mySet),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Object> store(@Valid @RequestParam(value = "admId", required = false) String admId, @RequestBody CategoryDTO categoryDTO) {
        Admin admin = adminService.findByAdminId(Long.parseLong(admId));
        Category category = new Category();
        category.setName(categoryDTO.getName());
        category.setAdmin(admin);
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new CategoryDTO(categoryService.create(category))),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<Object> getDetail(@PathVariable String id) {
        Category category = categoryService.findByCategoryId(Long.parseLong(id));
        if (category == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new CategoryDTO(categoryService.findByCategoryId(Long.parseLong(id)))),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<Object> update(@Valid @PathVariable String id, @RequestBody CategoryDTO updateCategoryDTO) {
        Category existCategory = categoryService.findByCategoryId(Long.parseLong(id));
        if (existCategory == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        existCategory.setName(updateCategoryDTO.getName());
        existCategory.setUpdatedAt(Calendar.getInstance().getTimeInMillis());
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new CategoryDTO(categoryService.update(existCategory))),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<Object> delete(@PathVariable String id) {
        Category deleteCategory = categoryService.findByCategoryId(Long.parseLong(id));
        if (deleteCategory == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        categoryService.delete(deleteCategory);
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Simple Success"),
                HttpStatus.OK);
    }
}
