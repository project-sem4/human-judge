package com.example.demo.endpoint;

import com.example.demo.dto.*;
import com.example.demo.dto.context.AdminContext;
import com.example.demo.dto.context.EmployeeContext;
import com.example.demo.dto.context.StudentContext;
import com.example.demo.dto.context.TeacherContext;
import com.example.demo.entity.*;
import com.example.demo.service.*;
import com.example.demo.util.JsonResponse;
import com.example.demo.util.StringUtil;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/login")
public class LoginEndpoint {
    @Autowired
    AccountService accountService;
    @Autowired
    StudentService studentService;
    @Autowired
    AdminService adminService;
    @Autowired
    EmployeeService employeeService;
    @Autowired
    TeacherService teacherService;
    StringUtil stringUtil = new StringUtil();

    // client gửi lên username và password, check thông tin nếu okie thì trả về credential.
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Object> login(@RequestBody AccountDTO accountDTO) {

        // check account tồn tại ko
        Account account = accountService.findAccountByUsername(accountDTO.getUsername());
        if (account == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        if (!BCrypt.checkpw(accountDTO.getPassword(), account.getPassword())) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.UNAUTHORIZED.value())
                    .setMessage("UNAUTHORIZED"),
                    HttpStatus.UNAUTHORIZED);
        }

        Credential credential = accountService.generateCredential(account);
        if (credential == null) {
            return null;
        } else {
            switch (stringUtil.convertIntRoleToString(credential.getAccount().getRole())) {
                case "Student":
                    Student student = studentService.findByAccountId(credential.getAccount().getId());
                    return new ResponseEntity<>(new StudentContext(
                            new AccountDTO(account),
                            new StudentDTO(student),
                            new CredentialDTO(credential)),
                            HttpStatus.OK);
                case "Admin":
                    Admin admin = adminService.findByAccountId(credential.getAccount().getId());
                    return new ResponseEntity<>(new AdminContext(
                            new AccountDTO(account),
                            new AdminDTO(admin),
                            new CredentialDTO(credential)),
                            HttpStatus.OK);
                case "Manager":
                    Employee employee = employeeService.findByAccountId(credential.getAccount().getId());
                    return new ResponseEntity<>(new EmployeeContext(
                            new AccountDTO(account),
                            new EmployeeDTO(employee),
                            new CredentialDTO(credential)),
                            HttpStatus.OK);
                case "Teacher":
                    Teacher teacher = teacherService.finByAccountId(credential.getAccount().getId());
                    return new ResponseEntity<>(new TeacherContext(
                            new AccountDTO(account),
                            new TeacherDTO(teacher),
                            new CredentialDTO(credential)),
                            HttpStatus.OK);
                default:
                    return null;
            }
        }
    }
}
