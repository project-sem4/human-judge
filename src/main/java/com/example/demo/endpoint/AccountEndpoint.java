package com.example.demo.endpoint;

import com.example.demo.dto.*;
import com.example.demo.dto.context.AdminContext;
import com.example.demo.dto.context.EmployeeContext;
import com.example.demo.dto.context.StudentContext;
import com.example.demo.dto.context.TeacherContext;
import com.example.demo.entity.*;
import com.example.demo.service.*;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import com.example.demo.util.JsonResponse;
import com.example.demo.util.StringUtil;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/_api/v1/accounts")
public class AccountEndpoint {

    private static Logger LOGGER = Logger.getLogger(AccountEndpoint.class.getSimpleName());

    @Autowired
    AccountService accountService;
    @Autowired
    StudentService studentService;
    @Autowired
    AdminService adminService;
    @Autowired
    EmployeeService employeeService;
    @Autowired
    TeacherService teacherService;
    @Autowired
    CloudinaryService cloudinaryService;

    StringUtil stringUtil = new StringUtil();

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Object> getList(
            @RequestParam(value = "keyword", required = false) String keyword,
            @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to) {
        Specification specification = Specification.where(null);
        if (keyword != null && keyword.length() > 0) {
            specification = specification
                    .and(new ObjectSpecification(new SearchCriteria("username", ":", keyword)))
                    .and(new ObjectSpecification(new SearchCriteria("email", ":", keyword)))
                    .or(new ObjectSpecification(new SearchCriteria("createdAt", ":", keyword)));
        }
        List<Account> accounts = accountService.getAccounts(specification);
        Set<AccountDTO> mySet = new HashSet<>();
        for (Account account : accounts) {
            mySet.add(new AccountDTO(account));
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Action success!")
                .setData(mySet),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<Object> getDetail(@PathVariable String id) {
        Account account = accountService.findByAccountId(Long.parseLong(id));
        if (account == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new AccountDTO(accountService.findByAccountId(Long.parseLong(id)))),
                HttpStatus.OK);

    }

    @RequestMapping(value = "/student", method = RequestMethod.POST)
    public ResponseEntity<Object> storeStudent(@Valid @RequestBody StudentDTO studentDTO) {
        String rollNumber = "D" + stringUtil.genDigits();
        Student student = new Student();
        student.setPhone(studentDTO.getPhone());
        student.setFullName(studentDTO.getFullName());
        student.setDob(studentDTO.getDob());
        student.setAddress(studentDTO.getAddress());
        boolean isFound = studentDTO.getImage().contains("base64");
        if (isFound == true) {
            String url = cloudinaryService.uploadFileBase64(studentDTO.getImage());
            student.setImage(url);
        } else {
            student.setImage(studentDTO.getImage());
        }
        student.setParentPhone(studentDTO.getParentPhone());
        student.setIdCard(studentDTO.getIdCard());
        student.setRollNumber(rollNumber);
        student.setStartYear(studentDTO.getStartYear());
        student.setEndYear(studentDTO.getEndYear());
        student.setGender(Student.Gender.findByValue(stringUtil.convertStringGenderToInt(studentDTO.getGender())));

        String generate = (stringUtil.generateUsernameFromFullName(student.getFullName()) + rollNumber).toLowerCase();

        Account account = new Account();
        account.setUsername(generate);
        account.setPassword(BCrypt.hashpw("Abc12345", BCrypt.gensalt()));
        account.setEmail(generate + "@fpt.edu.vn");
        account.setRole(Account.Role.STUDENT);

        student.setAccount(account);

        return new ResponseEntity<>(new StudentContext(
                new AccountDTO(accountService.register(account)),
                new StudentDTO(studentService.create(student))),
                HttpStatus.CREATED);

    }

    @RequestMapping(value = "/admin", method = RequestMethod.POST)
    public ResponseEntity<Object> storeAdmin(@Valid @RequestBody AdminDTO adminDTO) {
        Admin admin = new Admin();
        admin.setFullName(adminDTO.getFullName());
        admin.setPhone(adminDTO.getPhone());
        admin.setDob(adminDTO.getDob());
        admin.setAddress(adminDTO.getAddress());

        String generate = stringUtil.generateUsernameFromFullName(adminDTO.getFullName()).toLowerCase();

        Account account = new Account();
        if (accountService.findByUsernameAndEmail(generate, generate + "@fpt.edu.vn") != null) {
            int digit = stringUtil.genDigits();
            account.setUsername(generate + digit);
            account.setEmail(generate + digit + "@fpt.edu.vn");
        } else if (accountService.findByUsernameAndEmail(generate, generate + "@fpt.edu.vn") == null) {
            account.setUsername(generate);
            account.setEmail(generate + "@fpt.edu.vn");
        }
        account.setPassword(BCrypt.hashpw("Abc12345", BCrypt.gensalt()));
        account.setRole(Account.Role.ADMIN);

        admin.setAccount(account);

        Admin admin1 = adminService.create(admin);
        return new ResponseEntity<>(new AdminContext(
                new AccountDTO(accountService.register(account)),
                new AdminDTO(admin1)),
                HttpStatus.CREATED);

    }


    @RequestMapping(value = "/employee", method = RequestMethod.POST)
    public ResponseEntity<Object> storeEmployee(@Valid @RequestBody EmployeeDTO employeeDTO) {
        Employee employee = new Employee();
        employee.setFullName(employeeDTO.getFullName());
        employee.setPhone(employeeDTO.getPhone());
        employee.setDob(employeeDTO.getDob());
        boolean isFound = employeeDTO.getImage().contains("base64");
        if (isFound == true) {
            String url = cloudinaryService.uploadFileBase64(employeeDTO.getImage());
            employee.setImage(url);
        } else {
            employee.setImage(employeeDTO.getImage());
        }
        employee.setAddress(employeeDTO.getAddress());
        employee.setIdCard(employeeDTO.getIdCard());
        employee.setGender(Employee.Gender.findByValue(stringUtil.convertStringGenderToInt(employeeDTO.getGender())));

        String generate = stringUtil.generateUsernameFromFullName(employee.getFullName()).toLowerCase();

        Account account = new Account();
        if (accountService.findByUsernameAndEmail(generate, generate + "@fpt.edu.vn") != null) {
            int digit = stringUtil.genDigits();
            account.setUsername(generate + digit);
            account.setEmail(generate + digit + "@fpt.edu.vn");
        } else if (accountService.findByUsernameAndEmail(generate, generate + "@fpt.edu.vn") == null) {
            account.setUsername(generate);
            account.setEmail(generate + "@fpt.edu.vn");
        }
        account.setPassword(BCrypt.hashpw("Abc12345", BCrypt.gensalt()));
        account.setRole(Account.Role.MANAGER);

        employee.setAccount(account);

        Employee employee1 = employeeService.create(employee);
        return new ResponseEntity<>(new EmployeeContext(
                new AccountDTO(accountService.register(account)),
                new EmployeeDTO(employee1)),
                HttpStatus.CREATED);

    }

    @RequestMapping(value = "/teacher", method = RequestMethod.POST)
    public ResponseEntity<Object> storeTeacher(@Valid @RequestBody TeacherDTO teacherDTO) {
        Teacher teacher = new Teacher();
        teacher.setFullName(teacherDTO.getFullName());
        teacher.setPhone(teacherDTO.getPhone());
        teacher.setDob(teacherDTO.getDob());
        teacher.setGender(Teacher.Gender.findByValue(stringUtil.convertStringGenderToInt(teacherDTO.getGender())));
        teacher.setAddress(teacherDTO.getAddress());
        teacher.setIdCard(teacherDTO.getIdCard());
        boolean isFound = teacherDTO.getImage().contains("base64");
        if (isFound == true) {
            String url = cloudinaryService.uploadFileBase64(teacherDTO.getImage());
            teacher.setImage(url);
        } else {
            teacher.setImage(teacherDTO.getImage());
        }

        String generate = stringUtil.generateUsernameFromFullName(teacherDTO.getFullName()).toLowerCase();

        Account account = new Account();
        if (accountService.findByUsernameAndEmail(generate, generate + "@fpt.edu.vn") != null) {
            int digit = stringUtil.genDigits();
            account.setUsername(generate + digit);
            account.setEmail(generate + digit + "@fpt.edu.vn");
        } else if (accountService.findByUsernameAndEmail(generate, generate + "@fpt.edu.vn") == null) {
            account.setUsername(generate);
            account.setEmail(generate + "@fpt.edu.vn");
        }
        account.setPassword(BCrypt.hashpw("Abc12345", BCrypt.gensalt()));
        account.setRole(Account.Role.TEACHER);

        teacher.setAccount(account);

        return new ResponseEntity<>(new TeacherContext(
                new AccountDTO(accountService.register(account)),
                new TeacherDTO(teacherService.create(teacher))),
                HttpStatus.CREATED);

    }
}
