package com.example.demo.endpoint;

import com.example.demo.dto.ClassDTO;
import com.example.demo.dto.ScheduleDTO;
import com.example.demo.dto.ScheduleDTO2;
import com.example.demo.dto.TeacherDTO;
import com.example.demo.dto.context.ClassTeacherScheduleContext;
import com.example.demo.dto.context.ClazzScheduleContext;
import com.example.demo.entity.*;
import com.example.demo.service.*;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import com.example.demo.util.JsonResponse;
import com.example.demo.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/_api/v1/schedules")
public class ScheduleEndpoint {

    private static Logger LOGGER = Logger.getLogger(ScheduleEndpoint.class.getSimpleName());

    @Autowired
    ScheduleService scheduleService;
    @Autowired
    SubjectService subjectService;
    @Autowired
    TeacherService teacherService;
    @Autowired
    EmployeeService employeeService;
    @Autowired
    ClazzService clazzService;
    @Autowired
    RoomService roomService;
    @Autowired
    DayTypeService dayTypeService;
    @Autowired
    SlotService slotService;
    @Autowired
    StudentService studentService;
    StringUtil stringUtil = new StringUtil();

    Clazz clazz;
    Teacher teacher;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Object> getList(
            @RequestParam(value = "keyword", required = false) String keyword,
            @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to) {
        Specification specification = Specification.where(null);
        if (keyword != null && keyword.length() > 0) {
            specification = specification
                    .and(new ObjectSpecification(new SearchCriteria("name", ":", keyword)));
        }
        List<Schedule> schedules = scheduleService.getSchedule(specification);
        Set<ScheduleDTO2> mySet = new HashSet<>();
        for (Schedule schedule : schedules) {
            mySet.add(new ScheduleDTO2(schedule));
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Action success!")
                .setData(mySet),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Object> store(@RequestBody ScheduleDTO scheduleDTO) {
        Subject subject = subjectService.findBySubjectId(Long.parseLong(scheduleDTO.getSubjectId()));
        if (subject == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }

        Employee employee = employeeService.findByEmployeeId(Long.parseLong(scheduleDTO.getEmployeeId()));
        if (employee == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }

        teacher = teacherService.findByTeacherId(Long.parseLong(scheduleDTO.getTeacherId()));
        if (teacher == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }

        DayType dayType = dayTypeService.findByDayTypeId(Long.parseLong(scheduleDTO.getDayTypeId()));
        if (dayType == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }

        clazz = clazzService.findByClazzId(Long.parseLong(scheduleDTO.getClazzId()));
        if (clazz == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }

        Room room = roomService.findByRoomId(Long.parseLong(scheduleDTO.getRoomId()));
        if (room == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }

        List<String> generateDay = stringUtil.autoGenerateSchedule(scheduleDTO.getStartTime(), scheduleDTO.getEndTime(), subject.getPeriod(), dayType.getName());
        if (generateDay.size() == 0) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }

        List<Schedule> schedules = new ArrayList<>();
        String[] slotIds = scheduleDTO.getSlotId().split(",");
        for (String slotId : slotIds) {
            System.out.println(slotId);
            Slot slot = slotService.findBySlotId(Long.parseLong(slotId.trim()));
            if (slot == null) {
                return new ResponseEntity<>(new JsonResponse()
                        .setStatus(HttpStatus.NOT_FOUND.value())
                        .setMessage("Not found"),
                        HttpStatus.NOT_FOUND);
            }
            for (String sDate : generateDay) {
                Schedule schedule = new Schedule();
                schedule.setDate(sDate.trim());
                schedule.setClazz(clazz);
                schedule.setDayType(dayType);
                schedule.setEmployee(employee);
                schedule.setTeacher(teacher);
                schedule.setSubject(subject);
                schedule.setRoom(room);
                schedule.setSlot(slot);
                schedules.add(schedule);
            }
        }

        for (Schedule schedule : schedules) {
            scheduleService.create(schedule);
        }

        List<Schedule> schedules1 = scheduleService.getAllSchedule();
        Set<ScheduleDTO2> mySet = new HashSet<>();
        for (Schedule schedule : schedules1) {
            mySet.add(new ScheduleDTO2(schedule));
        }

        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.CREATED.value())
                .setMessage("Create success!")
                .setData(mySet),
                HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/activeSchedule")
    public ResponseEntity<Object> activeSchedule(@RequestBody ScheduleDTO scheduleDTO) {
        Schedule existSchedule = scheduleService.findByScheduleId(Long.parseLong(scheduleDTO.getId()));
        if (existSchedule == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        existSchedule.setStatus(Schedule.Status.ACTIVE);
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new ScheduleDTO(scheduleService.update(existSchedule))),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getScheduleByTeacher")
    public ResponseEntity<Object> getScheduleByTeacher(@RequestParam(value = "teacherId", required = false) String teacherId) {
        teacher = teacherService.findByTeacherId(Long.parseLong(teacherId));
        if (teacher == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        Set<ScheduleDTO2> mySet = new HashSet<>();
        for (Schedule schedule : teacher.getSchedules()) {
            mySet.add(new ScheduleDTO2(schedule));
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Action success!")
                .setData(mySet),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getScheduleByClass")
    public ResponseEntity<Object> getScheduleByClass(@RequestParam(value = "classId", required = false) String classId) {
        clazz = clazzService.findByClazzId(Long.parseLong(classId));
        if (clazz == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }

        Set<ScheduleDTO> mySet = new HashSet<>();
        for (Schedule schedule : clazz.getSchedules()) {
            mySet.add(new ScheduleDTO(schedule));
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Action success!")
                .setData(new ClazzScheduleContext(new ClassDTO(clazz), mySet)),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getScheduleByClassAndTeacher")
    public ResponseEntity<Object> getScheduleByClassAndTeacher(@RequestParam(value = "classId", required = false) String classId,
                                                               @RequestParam(value = "teacherId", required = false) String teacherId,
                                                               @RequestParam(value = "date", required = false) String date) {
        clazz = clazzService.findByClazzId(Long.parseLong(classId));
        if (clazz == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        teacher = teacherService.findByTeacherId(Long.parseLong(teacherId));
        if (teacher == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        List<Schedule> schedules = scheduleService.findByTeacherIdAndClassId(teacher.getId(), clazz.getId(), String.valueOf(date));
        if (schedules.size() == 0) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found lo"),
                    HttpStatus.NOT_FOUND);
        }

        Set<ScheduleDTO> mySet = new HashSet<>();
        for (Schedule schedule1 : schedules) {
            mySet.add(new ScheduleDTO(schedule1));
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Action success!")
                .setData(new ClassTeacherScheduleContext(new ClassDTO(clazz), new TeacherDTO(teacher), mySet)),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<Object> update(@Valid @PathVariable String id, @RequestBody ScheduleDTO updateScheduleDTO) {
        Schedule existSchedule = scheduleService.findByScheduleId(Long.parseLong(id));
        if (existSchedule == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        existSchedule.setDate(updateScheduleDTO.getDate());
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new ScheduleDTO(scheduleService.update(existSchedule))),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<Object> delete(@PathVariable String id) {
        Schedule deleteSchedule = scheduleService.findByScheduleId(Long.parseLong(id));
        if (deleteSchedule == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        scheduleService.delete(deleteSchedule);
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Simple Success"),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getScheduleByStudent")
    public ResponseEntity<Object> getScheduleByStudent(@RequestParam(value = "studentId", required = false) String studentId){
        Student student = studentService.findByStudentId(Long.parseLong(studentId));
        if (student == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        Set<ScheduleDTO2> mySet = new HashSet<>();
        for (Clazz clazz: student.getClasses()) {
            for (Schedule schedule: scheduleService.findByClass(clazz.getId())) {
                mySet.add(new ScheduleDTO2(schedule));
            }
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Get Success")
                .setData(mySet),
                HttpStatus.OK);
    }
}
