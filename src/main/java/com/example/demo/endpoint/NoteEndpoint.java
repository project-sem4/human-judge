package com.example.demo.endpoint;

import com.example.demo.dto.NoteDTO;
import com.example.demo.entity.Note;
import com.example.demo.entity.Student;
import com.example.demo.entity.Teacher;
import com.example.demo.service.NoteService;
import com.example.demo.service.StudentService;
import com.example.demo.service.TeacherService;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import com.example.demo.util.JsonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/_api/v1/note")
public class NoteEndpoint {

    private static Logger LOGGER = Logger.getLogger(NoteEndpoint.class.getSimpleName());

    @Autowired
    NoteService noteService;
    @Autowired
    StudentService studentService;
    @Autowired
    TeacherService teacherService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Object> getList(
            @RequestParam(value = "keyword", required = false) String keyword,
            @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to) {
        Specification specification = Specification.where(null);
        if (keyword != null && keyword.length() > 0) {
            specification = specification
                    .and(new ObjectSpecification(new SearchCriteria("note", ":", keyword)));
        }
        List<Note> notes = noteService.getNotes(specification);
        Set<NoteDTO> mySet = new HashSet<>();
        for (Note note : notes) {
            mySet.add(new NoteDTO(note, note.getStudent(), note.getTeacher()));
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Action success!")
                .setData(mySet),
                HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Object> store(@Valid
                                        @RequestParam(value = "studentId", required = false) String studentId,
                                        @RequestParam(value = "teacherId", required = false) String teacherId,
                                        @RequestBody NoteDTO noteDTO) {
        Student student = studentService.findByStudentId(Long.parseLong(studentId));
        Teacher teacher = teacherService.findByTeacherId(Long.parseLong(teacherId));
        Note note = new Note();
        note.setNote(noteDTO.getNote());
        note.setStudent(student);
        note.setTeacher(teacher);
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new NoteDTO(noteService.create(note))),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<Object> getDetail(@PathVariable String id) {
        Note noteDetail = noteService.findByNoteId(Long.parseLong(id));
        if (noteDetail == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new NoteDTO(noteService.findByNoteId(Long.parseLong(id)), noteDetail.getStudent(), noteDetail.getTeacher())),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<Object> update(@Valid @PathVariable String id, @RequestBody NoteDTO updateNoteDTO) {
        Note existNote = noteService.findByNoteId(Long.parseLong(id));
        if (existNote == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        existNote.setNote(updateNoteDTO.getNote());
        existNote.setUpdatedAt(Calendar.getInstance().getTimeInMillis());
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new NoteDTO(noteService.update(existNote))),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<Object> delete(@PathVariable String id) {
        Note deleteNote = noteService.findByNoteId(Long.parseLong(id));
        if (deleteNote == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        noteService.delete(deleteNote);
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Simple Success"),
                HttpStatus.OK);
    }

}
