package com.example.demo.endpoint;

import com.example.demo.dto.MessageDTO;
import com.example.demo.service.SMSService;
import com.example.demo.util.JsonResponse;
import com.twilio.rest.api.v2010.account.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/_api/v1/sms")
public class SMSEndpoint {
    @Autowired
    private SMSService smsService;

    @RequestMapping(value = "/sendSMS", method = RequestMethod.POST)
    public ResponseEntity<Object> sendSMS(@RequestBody MessageDTO messageDTO) {
        List<Message> list = new ArrayList<>();
        for (String phone : messageDTO.getPhone().split(",")) {
            list.add(smsService.sendSMS(messageDTO.getMessage(), phone.trim()));
        }
         return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(list),
                HttpStatus.OK);
    }
}
