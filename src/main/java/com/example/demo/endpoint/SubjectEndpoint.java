package com.example.demo.endpoint;

import com.example.demo.dto.SubjectDTO;
import com.example.demo.entity.Semester;
import com.example.demo.entity.Subject;
import com.example.demo.service.SemesterService;
import com.example.demo.service.SubjectService;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import com.example.demo.util.JsonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping(value = "/_api/v1/subjects")
public class SubjectEndpoint {

    private static Logger LOGGER = Logger.getLogger(SubjectEndpoint.class.getSimpleName());

    @Autowired
    SubjectService subjectService;

    @Autowired
    SemesterService semesterService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Object> getList(
            @RequestParam(value = "keyword", required = false) String keyword,
            @RequestParam(value = "from", required = false) String from,
            @RequestParam(value = "to", required = false) String to) {
        Specification specification = Specification.where(null);
        if (keyword != null && keyword.length() > 0) {
            specification = specification
                    .and(new ObjectSpecification(new SearchCriteria("subject", ":", keyword)));
        }
        List<Subject> subjects = subjectService.getSubjects(specification);
        Set<SubjectDTO> mySet = new HashSet<>();
        for (Subject subject : subjects) {
            mySet.add(new SubjectDTO(subject));
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Action success!")
                .setData(mySet),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Object> store(@Valid @RequestParam(value = "semesterId", required = false) String semesterId, @RequestBody SubjectDTO subjectDTO) {
        Semester semester = semesterService.findBySemesterId(Long.parseLong(semesterId));
        Subject subject = new Subject();
        subject.setSubject(subjectDTO.getSubject());
        subject.setCode(subjectDTO.getCode());
        subject.setPeriod(Integer.parseInt(subjectDTO.getPeriod()));
        subject.setSemester(semester);

        Subject subject1 = subjectService.create(subject);
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new SubjectDTO(subject, subject.getSemester())),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<Object> getDetail(@PathVariable String id) {
        Subject subject = subjectService.findBySubjectId(Long.parseLong(id));
        if (subject == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new SubjectDTO(subject, subject.getSemester())),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<Object> update(@Valid @PathVariable String id,
                                         @RequestParam(value = "semesterId", required = false) String semesterId,
                                         @RequestBody SubjectDTO updateSubjectDTO) {
        Subject existSubject = subjectService.findBySubjectId(Long.parseLong(id));
        if (existSubject == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        Semester semester = semesterService.findBySemesterId(Long.parseLong(semesterId));
        System.out.println(semester);
        if (semester == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        existSubject.setCode(updateSubjectDTO.getCode());
        existSubject.setSubject(updateSubjectDTO.getSubject());
        existSubject.setPeriod(Integer.parseInt(updateSubjectDTO.getPeriod()));
        existSubject.setSemester(semester);
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Success")
                .setData(new SubjectDTO(subjectService.update(existSubject), existSubject.getSemester())),
                HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<Object> delete(@PathVariable String id) {
        Subject deleteSubject = subjectService.findBySubjectId(Long.parseLong(id));
        if (deleteSubject == null) {
            return new ResponseEntity<>(new JsonResponse()
                    .setStatus(HttpStatus.NOT_FOUND.value())
                    .setMessage("Not found"),
                    HttpStatus.NOT_FOUND);
        }
        subjectService.delete(deleteSubject);
        return new ResponseEntity<>(new JsonResponse()
                .setStatus(HttpStatus.OK.value())
                .setMessage("Simple Success"),
                HttpStatus.OK);
    }

}
