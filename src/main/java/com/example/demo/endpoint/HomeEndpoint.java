package com.example.demo.endpoint;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(maxAge = 3600)
@RestController
public class HomeEndpoint {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home() {
        return "Hello Spring Boot!";
    }
}