package com.example.demo.service;

import com.example.demo.entity.Account;
import com.example.demo.entity.Credential;
import com.example.demo.repository.AccountRepository;
import com.example.demo.repository.ClazzRepository;
import com.example.demo.repository.CredentialRepository;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import com.example.demo.util.StringUtil;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class AccountService {
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    CredentialRepository credentialRepository;

    StringUtil stringUtil = new StringUtil();

    public Account findAccountByUsername(String username) {
        Optional<Account> optionalAccount = accountRepository.findAccountByUsername(username);
        if (optionalAccount.isPresent()) {
            return optionalAccount.get();
        }
        return null;
    }

    public Credential login(String username, String password) {
        Optional<Account> optionalAccount = accountRepository.findAccountByUsername(username);
        if (optionalAccount.isPresent()) {
            String token = UUID.randomUUID().toString();
            Account account = optionalAccount.get();

            Credential credential = new Credential();
            credential.setAccessToken(token);
            credential.setAccount(account);
            credentialRepository.save(credential);
            return credential;
        }
        return null;
    }

    public Account register(Account account) {
        return accountRepository.save(account);
    }

    public Account findByAccountId(long id) {
        return accountRepository.findAccountById(id).orElse(null);
    }

    public Account findByUsernameAndEmail(String username, String email) {
        return accountRepository.findByUsernameAndEmail(username, email).orElse(null);
    }

    public List<Account> getAccounts(Specification specification) {
        specification = specification
                .and(new ObjectSpecification(new SearchCriteria("status", "!=", Account.Status.DELETED.getValue())));
        return accountRepository.findAll(specification);
    }

    public Credential generateCredential(Account account) {
        return credentialRepository.save(Credential.getInstance(account));
    }
}
