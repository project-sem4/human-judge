package com.example.demo.service;

import com.example.demo.entity.Admin;
import com.example.demo.repository.AdminRepository;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminService {
    @Autowired
    AdminRepository adminRepository;


    public Admin create(Admin admin){
        return adminRepository.save(admin);
    }

    public Admin findByAccountId(long id){
        return adminRepository.findAdminByAccount_Id(id).orElse(null);
    }

    public Admin findByAdminId(long id){
        return adminRepository.findAdminById(id).orElse(null);
    }

    public Page<Admin> adminsWithPaginate(int page, int limit) {
        return adminRepository.findAll(PageRequest.of(page - 1, limit));
    }

    public List<Admin> getAdmin(Specification specification) {
        specification = specification
                .and(new ObjectSpecification(new SearchCriteria("status", "!=", Admin.Status.DELETED.getValue())));
        return adminRepository.findAll(specification);
    }

    public Admin update(Admin admin) {
        return adminRepository.save(admin);
    }

    public boolean delete(Admin admin) {
        admin.setStatus(Admin.Status.DELETED);
        adminRepository.save(admin);
        return true;
    }
}
