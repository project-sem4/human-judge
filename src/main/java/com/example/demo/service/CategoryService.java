package com.example.demo.service;

import com.example.demo.entity.Category;
import com.example.demo.repository.CategoryRepository;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {
    @Autowired
    CategoryRepository categoryRepository;

    public Category create(Category category) {
        return categoryRepository.save(category);
    }

    public Category findByCategoryId(long id) {
        return categoryRepository.findCategoriesById(id).orElse(null);
    }

    public List<Category> getCategories(Specification specification) {
        specification = specification
                .and(new ObjectSpecification(new SearchCriteria("status", "!=", Category.Status.DELETED.getValue())));
        return categoryRepository.findAll(specification);
    }

    public Category update(Category category) {
        return categoryRepository.save(category);
    }

    public boolean delete(Category category) {
        category.setStatus(Category.Status.DELETED);
        categoryRepository.save(category);
        return true;
    }
}
