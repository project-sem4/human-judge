package com.example.demo.service;

import com.example.demo.entity.Attendance;
import com.example.demo.repository.AttendanceRepository;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AttendanceService {
    @Autowired
    AttendanceRepository attendanceRepository;

    public Attendance create(Attendance cAttendance) {
        return attendanceRepository.save(cAttendance);
    }

    public Attendance findByAttendanceId(long id) {
        return attendanceRepository.findAttendanceById(id).orElse(null);
    }

    public List<Attendance> getAttendances(Specification specification) {
        specification = specification
                .and(new ObjectSpecification(new SearchCriteria("status", "!=", Attendance.Status.DELETED.getValue())));
        return attendanceRepository.findAll(specification);
    }

    public List<Attendance> findAttendancesByStudent(long id){
        return attendanceRepository.findByStudentId(id);
    }

    public Attendance update(Attendance uAttendance) {
        return attendanceRepository.save(uAttendance);
    }

    public boolean delete(Attendance dAttendance) {
        dAttendance.setStatus(Attendance.Status.DELETED);
        attendanceRepository.save(dAttendance);
        return true;
    }

    public String attendanceStudent(Attendance attendance) {
        if (attendanceRepository.save(attendance) != null) {
            return "Success";
        } else {
            return null;
        }
    }

    public List<Attendance> findBySchedule( long scheduleId) {
        return attendanceRepository.findByScheduleId(scheduleId);
    }

    public String attendanceSchedule(Attendance attendance) {
        if (attendanceRepository.save(attendance) != null) {
            return "Success";
        } else {
            return null;
        }
    }

    public Attendance findByScheduleIdAndStudentId(long scheduleId, long studentId) {
        return attendanceRepository.findByScheduleIdAndStudentId(scheduleId, studentId).orElse(null);
    }

    public List<Attendance> findAttendancesByStudentAndStatus(long id){
        return attendanceRepository.findByStudentIdAndStatus(id, 12);
    }
}
