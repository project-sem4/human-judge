package com.example.demo.service;

import com.example.demo.entity.Clazz;
import com.example.demo.repository.ClazzRepository;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClazzService {
    @Autowired
    ClazzRepository clazzRepository;

    public Clazz create(Clazz cClazz) {
        return clazzRepository.save(cClazz);
    }

    public Clazz findByClazzId(long id) {
        return clazzRepository.findClazzById(id).orElse(null);
    }

    public List<Clazz> getClazzs(Specification specification) {
        specification = specification
                .and(new ObjectSpecification(new SearchCriteria("status", "!=", Clazz.Status.DELETED.getValue())));
        return clazzRepository.findAll(specification);
    }

//    public List<Clazz> findClassByTeacher(Long teacherId){
//        List<Clazz> clazzList = clazzRepository.findClazzByTeacher_Id(teacherId);
//        return clazzList;
//    }

    public Clazz update(Clazz uClazz) {
        return clazzRepository.save(uClazz);
    }

    public boolean delete(Clazz dClazz) {
        dClazz.setStatus(Clazz.Status.DELETED);
        clazzRepository.save(dClazz);
        return true;
    }

    public String classTeacher(Clazz clazz) {
        if (clazzRepository.save(clazz) != null) {
            return "Success";
        } else {
            return null;
        }
    }

    public String classStudent(Clazz clazz) {
        if (clazzRepository.save(clazz) != null) {
            return "Success";
        } else {
            return null;
        }
    }
}
