package com.example.demo.service;

import com.example.demo.entity.Article;
import com.example.demo.repository.NewRepository;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewService {

    @Autowired
    NewRepository newRepository;

    public List<Article> getByStatus(int status) {
        return newRepository.getAllByStatus(status);
    }

    public Article create(Article cArticle) {
        return newRepository.save(cArticle);
    }

    public Article findByNewId(long id) {
        return newRepository.findNewById(id).orElse(null);
    }

    public List<Article> getNews(Specification specification) {
        specification = specification
                .and(new ObjectSpecification(new SearchCriteria("status", "!=", Article.Status.DELETED.getValue())));
        return newRepository.findAll(specification);
    }

    public Article update(Article uArticle) {
        return newRepository.save(uArticle);
    }

    public boolean delete(Article dArticle) {
        dArticle.setStatus(Article.Status.DELETED);
        newRepository.save(dArticle);
        return true;
    }
}
