package com.example.demo.service;

import com.example.demo.entity.Schedule;
import com.example.demo.entity.Teacher;
import com.example.demo.repository.ScheduleRepository;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ScheduleService {
    @Autowired
    ScheduleRepository scheduleRepository;

    public List<Schedule> getAllSchedule() {
        List<Schedule> list = scheduleRepository.findAll();
        List<Schedule> filterList = new ArrayList<>();
        for (Schedule schedule : list) {
            if (schedule.getStatus() != Schedule.Status.DELETED.getValue()) {
                filterList.add(schedule);
            }
        }
        return filterList;
    }

    public Schedule create(Schedule schedule) {
        return scheduleRepository.save(schedule);
    }

    public Schedule findByScheduleId(long id) {
        return scheduleRepository.findScheduleById(id).orElse(null);
    }

    public List<Schedule> findByTeacherIdAndClassId(long teacherId, long clazzId, String date) {
        return scheduleRepository.findByClazzIdAndTeacherIdAndDate(teacherId, clazzId, date);
    }

    public List<Schedule> findByClass(long clazzId) {
        return scheduleRepository.findByClazzId(clazzId);
    }

    public List<Schedule> getSchedule(Specification specification) {
        specification = specification
                .and(new ObjectSpecification(new SearchCriteria("status", "!=", Schedule.Status.DELETED.getValue())));
        return scheduleRepository.findAll(specification);
    }

    public Schedule update(Schedule schedule) {
        return scheduleRepository.save(schedule);
    }

    public boolean delete(Schedule schedule) {
        schedule.setStatus(Schedule.Status.DELETED);
        scheduleRepository.save(schedule);
        return true;
    }

    public List<Schedule> findSchedulebyCurrentDateandClass(String currentDate, long id) {
        return scheduleRepository.findByDateAndClazzId(currentDate, id);
    }

    public List<Schedule> findScheduleBySubject(long id){
        return scheduleRepository.findBySubjectId(id);
    }
}
