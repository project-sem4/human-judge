package com.example.demo.service;

import com.example.demo.entity.Note;
import com.example.demo.repository.NoteRepository;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoteService {

    @Autowired
    NoteRepository noteRepository;

    public Note create(Note note) {
        return noteRepository.save(note);
    }

    public Note findByNoteId(long id) {
        return noteRepository.findNoteById(id).orElse(null);
    }

    public List<Note> getNotes(Specification specification) {
        specification = specification
                .and(new ObjectSpecification(new SearchCriteria("status", "!=", Note.Status.DELETED.getValue())));
        return noteRepository.findAll(specification);
    }

    public Note update(Note note) {
        return noteRepository.save(note);
    }

    public boolean delete(Note note) {
        note.setStatus(Note.Status.CLOSED);
        noteRepository.save(note);
        return true;
    }
}
