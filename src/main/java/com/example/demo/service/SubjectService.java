package com.example.demo.service;

import com.example.demo.entity.Subject;
import com.example.demo.repository.SubjectRepository;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubjectService {

    @Autowired
    SubjectRepository subjectRepository;

    public Subject create(Subject cSubject) {
        return subjectRepository.save(cSubject);
    }

    public Subject findBySubjectId(long id) {
        return subjectRepository.findSubjectById(id).orElse(null);
    }

    public List<Subject> getSubjects(Specification specification) {
        specification = specification
                .and(new ObjectSpecification(new SearchCriteria("status", "!=", Subject.Status.DELETED.getValue())));
        return subjectRepository.findAll(specification);
    }

    public Subject update(Subject uSubject) {
        return subjectRepository.save(uSubject);
    }

    public boolean delete(Subject dSubject) {
        dSubject.setStatus(Subject.Status.DELETED);
        subjectRepository.save(dSubject);
        return true;
    }
}
