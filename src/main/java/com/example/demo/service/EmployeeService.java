package com.example.demo.service;

import com.example.demo.entity.Employee;
import com.example.demo.repository.EmployeeRepository;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    @Autowired
    EmployeeRepository employeeRepository;

    public Employee create(Employee employee){
        return employeeRepository.save(employee);
    }

    public Employee findByAccountId(long id){
        return employeeRepository.findEmployeeByAccount_Id(id).orElse(null);
    }

    public Employee findByEmployeeId(long id){
        return employeeRepository.findEmployeeById(id).orElse(null);
    }

    public List<Employee> getEmployees(Specification specification) {
        specification = specification
                .and(new ObjectSpecification(new SearchCriteria("status", "!=", Employee.Status.DELETED.getValue())));
        return employeeRepository.findAll(specification);
    }

    public List<Employee> getEmployeesForCron() {
        return employeeRepository.findAll();
    }

    public Employee update(Employee employee) {
        return employeeRepository.save(employee);
    }

    public boolean delete(Employee employee) {
        employee.setStatus(Employee.Status.DELETED);
        employeeRepository.save(employee);
        return true;
    }

}
