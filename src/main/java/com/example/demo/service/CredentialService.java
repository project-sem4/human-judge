package com.example.demo.service;

import com.example.demo.entity.Account;
import com.example.demo.entity.Credential;
import com.example.demo.repository.AccountRepository;
import com.example.demo.repository.CredentialRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CredentialService {
    @Autowired
    CredentialRepository credentialRepository;
    @Autowired
    AccountRepository accountRepository;


    public User findByToken(String token) {
        Optional<Credential> optional = credentialRepository.findByAccessToken(token);
        if (optional.isPresent()) {
            long id = optional.get().getAccount().getId();
            Optional<Account> account = accountRepository.findById(id);
            if (!account.isPresent()) return null;

            User user = new User(account.get().getUsername(), account.get().getPassword(), true, true, true, true, AuthorityUtils.createAuthorityList("USER"));
            return user;
        }
        return null;
    }
}
