package com.example.demo.service;

import com.example.demo.entity.Slot;
import com.example.demo.repository.SlotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SlotService {

    @Autowired
    SlotRepository slotRepository;

    public Slot create(Slot slot) {
        return slotRepository.save(slot);
    }

    public Slot findBySlotId(long id) {
        return slotRepository.findSlotById(id).orElse(null);
    }

    public List getSlots(Specification specification) {
        return slotRepository.findAll(specification);
    }

    public Slot update(Slot slot) {
        return slotRepository.save(slot);
    }

    public String slotClass(Slot slot) {
        if (slotRepository.save(slot) != null) {
            return "Success";
        } else {
            return null;
        }
    }
}
