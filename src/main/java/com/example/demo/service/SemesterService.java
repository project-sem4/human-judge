package com.example.demo.service;

import com.example.demo.entity.Semester;
import com.example.demo.repository.SemesterRepository;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SemesterService {

    @Autowired
    SemesterRepository semesterRepository;

    public Semester create(Semester cSemester) {
        return semesterRepository.save(cSemester);
    }

    public Semester findBySemesterId(long id) {
        return semesterRepository.findSemesterById(id).orElse(null);
    }

    public List<Semester> getSemesters(Specification specification) {
        specification = specification
                .and(new ObjectSpecification(new SearchCriteria("status", "!=", Semester.Status.DELETED.getValue())));
        return semesterRepository.findAll(specification);
    }

    public Semester update(Semester uSemester) {
        return semesterRepository.save(uSemester);
    }

    public boolean delete(Semester dSemester) {
        dSemester.setStatus(Semester.Status.DELETED);
        semesterRepository.save(dSemester);
        return true;
    }
}
