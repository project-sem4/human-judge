package com.example.demo.service;

import com.example.demo.entity.Teacher;
import com.example.demo.repository.TeacherRepository;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherService {

    @Autowired
    TeacherRepository teacherRepository;

    public Teacher create(Teacher teacher){
        return teacherRepository.save(teacher);
    }

    public Teacher finByAccountId(long id){
        return teacherRepository.findTeacherByAccount_Id(id).orElse(null);
    }

    public Teacher findByTeacherId(long id){
        return teacherRepository.findTeacherById(id).orElse(null);
    }

    public List<Teacher> getTeachers(Specification specification) {
        specification = specification
                .and(new ObjectSpecification(new SearchCriteria("status", "!=", Teacher.Status.DELETED.getValue())));
        return teacherRepository.findAll(specification);
    }

    public Teacher update(Teacher teacher) {
        return teacherRepository.save(teacher);
    }

    public boolean delete(Teacher teacher) {
        teacher.setStatus(Teacher.Status.DELETED);
        teacherRepository.save(teacher);
        return true;
    }

    public String teacherClass(Teacher teacher) {
        if (teacherRepository.save(teacher) != null) {
            return "Success";
        } else {
            return null;
        }
    }
}
