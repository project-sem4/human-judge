package com.example.demo.service;

import com.example.demo.entity.Student;
import com.example.demo.repository.StudentRepository;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {

    @Autowired
    StudentRepository studentRepository;

    public Student create(Student student) {
        return studentRepository.save(student);
    }

    public Student findByAccountId(long id) {
        return studentRepository.findStudentByAccount_Id(id).orElse(null);
    }

    public Student findByStudentId(long id) {
        return studentRepository.findStudentById(id).orElse(null);
    }

    public List<Student> getStudents(Specification specification) {
        specification = specification
                .and(new ObjectSpecification(new SearchCriteria("status", "!=", Student.Status.DELETED.getValue())));
        return studentRepository.findAll(specification);
    }

    public List<Student> getStudentsForCron() {
        return studentRepository.findAll();
    }

    public Student update(Student student) {
        return studentRepository.save(student);
    }

    public boolean delete(Student student) {
        student.setStatus(Student.Status.DELETED);
        studentRepository.save(student);
        return true;
    }

    public String studentClass(Student student) {
        if (studentRepository.save(student) != null) {
            return "Success";
        } else {
            return null;
        }
    }
}
