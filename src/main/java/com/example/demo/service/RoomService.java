package com.example.demo.service;

import com.example.demo.entity.Room;
import com.example.demo.repository.RoomRepository;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomService {

    @Autowired
    RoomRepository roomRepository;

    public Room create(Room cRoom) {
        return roomRepository.save(cRoom);
    }

    public Room findByRoomId(long id) {
        return roomRepository.findRoomById(id).orElse(null);
    }

    public List<Room> getRooms(Specification specification) {
        specification = specification
                .and(new ObjectSpecification(new SearchCriteria("status", "!=", Room.Status.DELETED.getValue())));
        return roomRepository.findAll(specification);
    }

    public Room update(Room uRoom) {
        return roomRepository.save(uRoom);
    }

    public boolean delete(Room dRoom) {
        dRoom.setStatus(Room.Status.DELETED);
        roomRepository.save(dRoom);
        return true;
    }

}
