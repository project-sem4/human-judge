package com.example.demo.service;

import com.example.demo.entity.DayType;
import com.example.demo.repository.DayTypeRepository;
import com.example.demo.specification.ObjectSpecification;
import com.example.demo.specification.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DayTypeService {
    @Autowired
    DayTypeRepository dayTypeRepository;

    public DayType create(DayType cDayType){
        return dayTypeRepository.save(cDayType);
    }

    public DayType findByDayTypeId(long id){
        return  dayTypeRepository.findDayTypeById(id).orElse(null);
    }

    public List<DayType> getDayType(Specification specification) {
        specification = specification
                .and(new ObjectSpecification(new SearchCriteria("status", "!=", DayType.Status.DELETED.getValue())));
        return dayTypeRepository.findAll(specification);
    }

    public DayType update(DayType uDayType){
        return dayTypeRepository.save(uDayType);
    }

    public boolean delete(DayType dDayType) {
        dDayType.setStatus(DayType.Status.DELETED);
        dayTypeRepository.save(dDayType);
        return true;
    }
}
