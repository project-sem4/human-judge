package com.example.demo.service;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import org.springframework.stereotype.Service;

@Service
public class SMSServiceTwilio implements SMSService{
    // Find your Account Sid and Token at twilio.com/console
    public static final String ACCOUNT_SID = "AC42cb5013da00a2d90c0fc224a2102670";
    public static final String AUTH_TOKEN = "24fb2bf13460d09bcd1ac9877b5768ec";
    public static final String PHONE_TWILIO = "+19146183017";

    @Override
    public Message sendSMS(String cMessage, String parrentPhone) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message.creator(
                new com.twilio.type.PhoneNumber(parrentPhone),//The phone number you are sending text to
                new com.twilio.type.PhoneNumber(PHONE_TWILIO),//The Twilio phone number
                cMessage)
                .create();

        return message;
    }
}
