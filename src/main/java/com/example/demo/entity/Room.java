package com.example.demo.entity;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "room")
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NotNull(message = "Name cannot be null.")
    @Length(min = 4, message = "Name must not be less than 4 characters.")
    @Column(unique = true, name = "name")
    private String name;
    @Column(name = "created_at")
    private long createdAt;
    @Column(name = "updated_at")
    private long updatedAt;
    @Column(name = "deleted_at")
    private long deletedAt;
    @Column(name = "status")
    private int status;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "rooms", cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, orphanRemoval = true)
    private Set<Clazz> clazz = new HashSet<>();
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "room", cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, orphanRemoval = true)
    private Set<Schedule> schedule = new HashSet<>();

    public Room() {
        this.createdAt = Calendar.getInstance().getTimeInMillis();
        this.updatedAt = Calendar.getInstance().getTimeInMillis();
        this.status = Status.ENABLE.getValue();
    }

    public Room(String name, long updatedAt) {
        this.name = name;
        this.updatedAt = updatedAt;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public long getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(long deletedAt) {
        this.deletedAt = deletedAt;
    }

    public enum Status {
        ACTIVE(1), DEACTIVE(0), DELETED(-1), STUDYING(2), GRADUATED(3), RESERVE(4), WAITINGCLASS(5), WORKING(6), RETIRED(7), OPENNING(8), CLOSED(9), ENABLE(10), DISABLE(11), ABSENT(12), ATTENDED(13), DONE(14), PENDING(15), AVAILABLE(16), UNAVAILABLE(17);

        int value;

        Status(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Status findByValue(int value) {
            for (Status status :
                    Status.values()) {
                if (status.getValue() == value) {
                    return status;
                }
            }
            return null;
        }
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        if (status == null) {
            status = Status.ENABLE;
        }
        this.status = status.getValue();
    }

    public Set<Clazz> getClazz() {
        return clazz;
    }

    public void setClazz(Set<Clazz> clazz) {
        this.clazz = clazz;
    }

    public Set<Schedule> getSchedule() {
        return schedule;
    }

    public void setSchedule(Set<Schedule> schedule) {
        this.schedule = schedule;
    }
}
