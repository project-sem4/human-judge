package com.example.demo.entity;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "account")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NotNull(message = "Name cannot be null.")
    @Length(min = 2, message = "Name must not be less than 2 characters.")
    @Column(unique = true, name = "username")
    private String username;
    @NotNull(message = "Password cannot be null.")
    @Length(min = 6, message = "Password must be equal or grater than 6 characters")
    private String password;
    @NotNull(message = "Email cannot be null.")
    @Email(message = "Invalid email type")
    @Column(name = "email", unique = true)
    private String email;
    private String avatar;
    @Column(name = "created_at")
    private long createdAt;
    @Column(name = "updated_at")
    private long updatedAt;
    @Column(name = "deleted_at")
    private long deletedAt;
    private int status;
    private int role;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account", cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, orphanRemoval = true)
    private Set<Credential> credentials;

    public Account() {
        this.createdAt = Calendar.getInstance().getTimeInMillis();
        this.updatedAt = Calendar.getInstance().getTimeInMillis();
        this.status = Status.ACTIVE.getValue();
    }

    public Account(long id, String username, String password, String email, String avatar, long createdAt, long updatedAt, long deletedAt, int status, int role) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.avatar = avatar;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.deletedAt = deletedAt;
        this.status = status;
        this.role = role;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public long getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(long deletedAt) {
        this.deletedAt = deletedAt;
    }

    public enum Status {
        ACTIVE(1), DEACTIVE(0), DELETED(-1), STUDYING(2), GRADUATED(3), RESERVE(4), WAITINGCLASS(5), WORKING(6), RETIRED(7), OPENNING(8), CLOSED(9), ENABLE(10), DISABLE(11), ABSENT(12), ATTENDED(13), DONE(14), PENDING(15), AVAILABLE(16), UNAVAILABLE(17);

        int value;

        Status(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Status findByValue(int value) {
            for (Status status :
                    Status.values()) {
                if (status.getValue() == value) {
                    return status;
                }
            }
            return null;
        }
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        if (status == null) {
            status = Status.DEACTIVE;
        }
        this.status = status.getValue();
    }

    public enum Role {
        ADMIN(2), STUDENT(1), MANAGER(3), TEACHER(4);

        int value;

        Role(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Role findByValue(int value) {
            for (Role role :
                    Role.values()) {
                if (role.getValue() == value) {
                    return role;
                }
            }
            return null;
        }

    }

    public int getRole() {
        return role;
    }

    public void setRole(Role role) {
        if (role == null) {
            role = Role.STUDENT;
        }
        this.role = role.getValue();
    }

    public Set<Credential> getCredentials() {
        return credentials;
    }

    public void setCredentials(Set<Credential> credentials) {
        this.credentials = credentials;
    }
}
