package com.example.demo.entity;

import javax.persistence.*;
import java.util.Calendar;
import java.util.UUID;

@Entity
public class Credential {

    @Id
    private String accessToken;
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "account_id")
    private Account account;
    private long tokenExpiredAt;
    private long createdAt;
    private long updatedAt;

    public Credential() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 7);
        this.createdAt = Calendar.getInstance().getTime().getTime();
        this.updatedAt = Calendar.getInstance().getTime().getTime();
        this.tokenExpiredAt = calendar.getTime().getTime();
    }

    public static Credential getInstance(Account account) {
        String token = UUID.randomUUID().toString();
        Credential credential = new Credential();
        credential.setAccessToken(token);
        credential.setAccount(account);
        return credential;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public long getTokenExpiredAt() {
        return tokenExpiredAt;
    }

    public void setTokenExpiredAt(long tokenExpiredAt) {
        this.tokenExpiredAt = tokenExpiredAt;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }
}
