package com.example.demo.entity;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NotNull(message = "FullName cannot be null.")
    @Length(min = 4, message = "FullName must not be less than 4 characters.")
    @Column(name = "full_name")
    private String fullName;
    @NotNull(message = "Phone numbers cannot be null.")
    @Length(min = 8, max = 14, message = "Phone numbers must be 8 characters  and less than 14 characters.")
    @Column(name = "phone", nullable = false)
    private String phone;
    @NotNull(message = "Date of birth cannot be null.")
    @Column(name = "date_of_birth")
    private String dob;
    @NotNull(message = "Image cannot be null.")
    @Column(name = "image", nullable = false)
    private String image;
    @NotNull(message = "Address cannot be null.")
    @Length(min = 4, message = "Image must not be less than 4 characters.")
    private String address;
    @NotNull(message = "Id card cannot be null.")
    @Length(min = 8, max = 12, message = "Id card must be 12 characters.")
    @Column(name = "id_card", unique = true, nullable = false)
    private String idCard;
    @NotNull(message = "Gender cannot be null.")
    private int gender;
    private int status;
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "account_id")
    private Account account;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "employee", cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, orphanRemoval = true)
    private Set<Schedule> schedules = new HashSet<>();

    public Employee() {
        this.status = Status.WORKING.getValue();
    }

    public Employee(long id, String fullName, String phone, String dob, String image, String address, String idCard, int gender, int status, Account account) {
        this.id = id;
        this.fullName = fullName;
        this.phone = phone;
        this.dob = dob;
        this.image = image;
        this.address = address;
        this.idCard = idCard;
        this.gender = gender;
        this.status = status;
        this.account = account;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public enum Gender {
        MALE(1), FEMALE(0), OTHERS(2);
        int value;

        Gender(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Gender findByValue(int value) {
            for (Gender gender :
                    Gender.values()) {
                if (gender.getValue() == value) {
                    return gender;
                }
            }
            return null;
        }
    }

    public int getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        if (gender == null) {
            gender = Gender.FEMALE;
        }
        this.gender = gender.getValue();
    }

    public enum Status {
        ACTIVE(1), DEACTIVE(0), DELETED(-1), STUDYING(2), GRADUATED(3), RESERVE(4), WAITINGCLASS(5), WORKING(6), RETIRED(7), OPENNING(8), CLOSED(9), ENABLE(10), DISABLE(11), ABSENT(12), ATTENDED(13), DONE(14), PENDING(15), AVAILABLE(16), UNAVAILABLE(17);

        int value;

        Status(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Status findByValue(int value) {
            for (Status status :
                    Status.values()) {
                if (status.getValue() == value) {
                    return status;
                }
            }
            return null;
        }
    }

    public void setStatus(Status status) {
        if (status == null) {
            status = Status.WORKING;
        }
        this.status = status.getValue();
    }

    public int getStatus() {
        return status;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Set<Schedule> getSchedules() {
        return schedules;
    }

    public void setSchedules(Set<Schedule> schedules) {
        this.schedules = schedules;
    }
}
