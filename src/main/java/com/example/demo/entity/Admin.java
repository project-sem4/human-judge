package com.example.demo.entity;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.*;

@Entity
@Table(name = "admin")
public class Admin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NotNull(message = "FullName cannot be null.")
    @Length(min = 4, message = "FullName must not be less than 4 characters.")
    @Column(name = "full_name")
    private String fullName;
    @NotNull(message = "Phone numbers cannot be null.")
    @Length(min = 8, max = 14, message = "Phone numbers must be from 8 to 14 characters.")
    private String phone;
    @NotNull(message = "Date of birth cannot be null.")
    @Column(name = "date_of_birth")
    private String dob;
    @NotNull(message = "Address cannot be null.")
    @Length(min = 4, message = "Image must not be less than 4 characters.")
    private String address;
    private int status;
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "account_id")
    private Account account;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "admin", cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, orphanRemoval = true)
    private Set<Category> categories = new HashSet<>();
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "admin", cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, orphanRemoval = true)
    private Set<Article> articles = new HashSet<>();

    public Admin() {
        this.status = Status.ACTIVE.getValue();
    }

    public Admin(long id, String fullName, String phone, String dob, String address, int status, Account account, Set<Category> categories, Set<Article> articles) {
        this.id = id;
        this.fullName = fullName;
        this.phone = phone;
        this.dob = dob;
        this.address = address;
        this.status = status;
        this.account = account;
        this.categories = categories;
        this.articles = articles;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getStatus() {
        return status;
    }

    public enum Status {
        ACTIVE(1), DEACTIVE(0), DELETED(-1), STUDYING(2), GRADUATED(3), RESERVE(4), WAITINGCLASS(5), WORKING(6), RETIRED(7), OPENNING(8), CLOSED(9), ENABLE(10), DISABLE(11), ABSENT(12), ATTENDED(13), DONE(14), PENDING(15), AVAILABLE(16), UNAVAILABLE(17);

        int value;

        Status(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Status findByValue(int value) {
            for (Status status :
                    Status.values()) {
                if (status.getValue() == value) {
                    return status;
                }
            }
            return null;
        }
    }

    public Set<Article> getArticles() {
        return articles;
    }

    public void setArticles(Set<Article> articles) {
        this.articles = articles;
    }

    public void setStatus(Status status) {
        if (status == null) {
            status = Status.ACTIVE;
        }
        this.status = status.getValue();
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }

    public Set<Article> getNews() {
        return articles;
    }

    public void setNews(Set<Article> articles) {
        this.articles = articles;
    }
}
