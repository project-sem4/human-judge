package com.example.demo.entity;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.*;

@Entity
@Table(name = "student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NotNull(message = "FullName cannot be null.")
    @Length(min = 4, message = "FullName must not be less than 4 characters.")
    @Column(name = "full_name")
    private String fullName;
    @NotNull(message = "Phone numbers cannot be null.")
    @Length(min = 8, max = 14, message = "Phone numbers must be 8 characters and less than 14 characters.")
    private String phone;
    @NotNull(message = "Date of birth cannot be null.")
    @Column(name = "date_of_birth")
    private String dob;
    @NotNull(message = "Address cannot be null.")
    @Length(min = 4, message = "Image must not be less than 4 characters.")
    private String address;
    @NotNull(message = "Image cannot be null.")
    @Column(name = "image", nullable = false, columnDefinition = "TEXT", length = 1024)
    private String image;
    @NotNull(message = "ParentPhone numbers cannot be null.")
    @Length(min = 10, max = 14, message = "ParentPhone numbers must be 10 characters and less than 14 characters.")
    @Column(name = "parent_phone", nullable = false)
    private String parentPhone;
    @NotNull(message = "Id card cannot be null.")
    @Length(min = 8, max = 12, message = "Id card must be 12 characters.")
    @Column(name = "id_card", unique = true, nullable = false)
    private String idCard;
    @NotNull(message = "Gender cannot be null.")
    private int gender;
    @Column(name = "training_status")
    private int trainingStatus;
    @NotNull(message = "RollNumber cannot be null.")
    @Length(min = 6, message = "RollNumber must not be less than 6 characters.")
    @Column(name = "roll_number", unique = true, nullable = false)
    private String rollNumber;
    private int status;
    @Column(name = "start_year")
    private String startYear;
    @Column(name = "end_year")
    private String endYear;
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "account_id")
    private Account account;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "student_class", joinColumns = {@JoinColumn(name = "student_id")}, inverseJoinColumns = {@JoinColumn(name = "class_id")})
    private Set<Clazz> classes = new HashSet<>();
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "student", cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, orphanRemoval = true)
    private Set<Note> notes = new HashSet<>();
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "student", cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, orphanRemoval = true)
    private Set<Attendance> attendances = new HashSet<>();

    public Student() {
        this.status = Status.WAITINGCLASS.getValue();
        this.trainingStatus = TrainingStatus.DEACTIVE.getValue();
    }

    public Student(long id, String fullName, String phone, String dob, String address, String image, String parentPhone, String idCard, int gender, String rollNumber, int status, String startYear, String endYear, Account account, Set<Clazz> classes, Set<Note> notes) {
        this.id = id;
        this.fullName = fullName;
        this.phone = phone;
        this.dob = dob;
        this.address = address;
        this.image = image;
        this.parentPhone = parentPhone;
        this.idCard = idCard;
        this.gender = gender;
        this.rollNumber = rollNumber;
        this.status = status;
        this.startYear = startYear;
        this.endYear = endYear;
        this.account = account;
        this.classes = classes;
        this.notes = notes;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getParentPhone() {
        return parentPhone;
    }

    public void setParentPhone(String parentPhone) {
        this.parentPhone = parentPhone;
    }

    public enum TrainingStatus {
        ACTIVE(1), DEACTIVE(0);

        int value;

        TrainingStatus(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static TrainingStatus findByValue(int value) {
            for (TrainingStatus trainingStatus :
                    TrainingStatus.values()) {
                if (trainingStatus.getValue() == value) {
                    return trainingStatus;
                }
            }
            return null;
        }
    }

    public void setTrainingStatus(int trainingStatus) {
        this.trainingStatus = trainingStatus;
    }

    public int getTrainingStatus() {
        return trainingStatus;
    }

    public void setTrainingStatus(TrainingStatus trainingStatus) {
        if (trainingStatus == null) {
            trainingStatus = TrainingStatus.ACTIVE;
        }
        this.trainingStatus = trainingStatus.getValue();
    }

    public Set<Attendance> getAttendances() {
        return attendances;
    }

    public void setAttendances(Set<Attendance> attendances) {
        this.attendances = attendances;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        if (gender == null) {
            gender = Gender.FEMALE;
        }
        this.gender = gender.getValue();
    }

    public enum Gender {
        MALE(1), FEMALE(0), OTHERS(2);
        int value;

        Gender(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Gender findByValue(int value) {
            for (Gender gender :
                    Gender.values()) {
                if (gender.getValue() == value) {
                    return gender;
                }
            }
            return null;
        }
    }

    public String getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    public enum Status {
        ACTIVE(1), DEACTIVE(0), DELETED(-1), STUDYING(2), GRADUATED(3), RESERVE(4), WAITINGCLASS(5), WORKING(6), RETIRED(7), OPENNING(8), CLOSED(9), ENABLE(10), DISABLE(11), ABSENT(12), ATTENDED(13), DONE(14), PENDING(15), AVAILABLE(16), UNAVAILABLE(17);

        int value;

        Status(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Status findByValue(int value) {
            for (Status status :
                    Status.values()) {
                if (status.getValue() == value) {
                    return status;
                }
            }
            return null;
        }
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        if (status == null) {
            status = Status.WAITINGCLASS;
        }
        this.status = status.getValue();
    }

    public String getStartYear() {
        return startYear;
    }

    public void setStartYear(String startYear) {
        this.startYear = startYear;
    }

    public String getEndYear() {
        return endYear;
    }

    public void setEndYear(String endYear) {
        this.endYear = endYear;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Set<Clazz> getClasses() {
        return classes;
    }

    public void setClasses(Set<Clazz> classes) {
        this.classes = classes;
    }

    public Set<Note> getNotes() {
        return notes;
    }

    public void setNotes(Set<Note> notes) {
        this.notes = notes;
    }
}